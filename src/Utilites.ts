﻿module TurnBasedBattles {

    export const COLOR_CLEAR_NUM: number = 0xFFFFFF;
    export const COLOR_HOVER_YELLOW_NUM: number = 0xffe484;
    export const COLOR_HOVER_RED_NUM2: number = 0xff2222;
    export const COLOR_HOVER_GREEN_NUM3: number = 0x22ff22;
    export const COLOR_HOVER_ORANGE: number = 0xffb12b;

    export const BOOT_STATE: string = "Boot";
    export const TOWN_STATE: string = "Town";
    export const PRELOAD_STATE: string = "Preload";
    export const MAP_STATE: string = "Map";
    export const MENU_STATE: string = "Menu";
    export const FIELD_BATTLE_STATE: string = "FieldBattle";

    export const HIDE_FROM_SCREEN: number = -1000;
    export const START_MONEY: number = 990;

    export const UNIT_SELECTION_POS_X: number = 50;
    export const UNIT_SELECTION_POS_Y: number = 50;

    export const COLOR_ORANGE: string = "#f4b042";
    export const COLOR_YELLOW: string = "#ffFF0F";
    export const COLOR_GOLD: string = "#f2d254";

    export const GOLD_STR: string = "GOLD: ";

    export const FONT_ACME: string = "30px Acme";
    export const FONT_BERKSHIRE: string = "50px Berkshire Swash";

    export const GOLD_KNIGHT_STR: string = "Gold Knight";
    export const ELF_MAGE_STR: string = "Elf Mage";
    export const WOMAN_ARCHER_STR: string = "Woman Archer";

    export const PUNIT_ARRAY_X: number[] = [
        250,     //0
        250,     //2
        250,     //4
        100,     //6
        100,     //8
        100,     //10
    ];

    export const PUNIT_ARRAY_Y: number[] = [
        450,     //1
        550,     //3
        650,     //5
        450,     //7
        550,     //9
        650      //11
    ];

    export const EUNIT1_POS_X: number = 850;
    export const EUNIT1_POS_Y: number = 450;
    export const EUNIT2_POS_X: number = 850;
    export const EUNIT2_POS_Y: number = 550;
    export const EUNIT3_POS_X: number = 850;
    export const EUNIT3_POS_Y: number = 650;
    export const EUNIT4_POS_X: number = 1000;
    export const EUNIT4_POS_Y: number = 450;
    export const EUNIT5_POS_X: number = 1000;
    export const EUNIT5_POS_Y: number = 550;
    export const EUNIT6_POS_X: number = 1000;
    export const EUNIT6_POS_Y: number = 650;

    export const PSLOT_ARRAY_X: number[] = [
        160,     //1
        160,     //3
        160,     //5
        60,      //7
        60,      //9
        60       //11
    ];

    export const PSLOT_ARRAY_Y: number[] = [
        45,      //1
        145,     //3
        245,     //5
        45,      //7
        145,     //9
        245      //11
    ];


    export const E1SLOT_POS_X: number = 1110;
    export const E1SLOT_POS_Y: number = 45;
    export const E2SLOT_POS_X: number = 1110;
    export const E2SLOT_POS_Y: number = 145;
    export const E3SLOT_POS_X: number = 1110;
    export const E3SLOT_POS_Y: number = 245;
    export const E4SLOT_POS_X: number = 1210;
    export const E4SLOT_POS_Y: number = 45;
    export const E5SLOT_POS_X: number = 1210;
    export const E5SLOT_POS_Y: number = 145;
    export const E6SLOT_POS_X: number = 1210;
    export const E6SLOT_POS_Y: number = 245;

    export const MOVEMENT: number = 1250;
    export const BUFFER: number = 50;

    export const MAP_WIDTH: number = 4641;
    export const MAP_HEIGHT: number = 720;

    export const SIZE_ARCHER: number = 0.75;
    export const SIZE_KNIGHT: number = 1;
    export const SIZE_MAGE: number = 0.55;
    export const SIZE_TROLL: number = 1.3;
    export const SIZE_ELF_ARCHER: number = 1.3;
    export const SIZE_KNIGHT_AXE: number = 0.68;
    export const SIZE_KNIGHT_SPEAR: number = 0.60;
    export const SIZE_ORC: number = 1.3;
    export const SIZE_BOSS: number = 2.5;
    export const SIZE_HP: number = 0.27;
    export const SIZE_DMG: number = 40;
}
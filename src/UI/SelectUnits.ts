﻿module TurnBasedBattles {

    const TOTAL_UNIT_SLOTS: number = 6;

    export class SelectUnits extends Phaser.Sprite {

        public game: Phaser.Game;
        public unitFormation: Array<Unit>;

        private unitSelectionSprite: Phaser.Sprite;
        private unitIcons: Array<Unit>;
        private clickerSprites: Array<Phaser.Sprite>;
        private unitInfoText: Phaser.Text;

        constructor(game: Phaser.Game, x: number, y: number, newUnitFormation: Array<Unit>) {
            super(game, x, y);
            this.game = game;
            this.unitSelectionSprite = this.game.add.sprite(UNIT_SELECTION_POS_X, UNIT_SELECTION_POS_Y, "unitSelection");
            this.unitSelectionSprite.alpha = 0;
            this.unitSelectionSprite.x = HIDE_FROM_SCREEN;
            this.clickerSprites = [];
            this.unitIcons = [];
            this.unitFormation = [];

            if (newUnitFormation === undefined) {
                this.drawAllIconsAndText(this.unitIcons);
                this.loadClickers();
                this.loadDefaultIcons();
                //console.log("draw icons and text unit icons UNDEFINED");
            } else {
                this.drawAllIconsAndText(this.unitIcons);
                this.loadClickers();
                this.loadDefaultIcons();
                //console.log("1. These are the generated units: ");
                //console.log(newUnitFormation);
                this.generateUnits(newUnitFormation);
                //console.log("draw icons and text not Undefined");
                //console.log("2. These are the generated units: ");
                //console.log(newUnitFormation);
            }
        }

        public displayUnitSelectionMenu(open: boolean, unitFormation: Array<Unit>): void {
            if (open) {
                if (unitFormation === undefined) {
                    this.showIcons(this.unitIcons, false);
                    //console.log("Frst time from select units and its UNDEFINED");
                } else {
                    unitFormation = unitFormation.concat(this.unitIcons);
                    this.showIcons(unitFormation, true);
                    //console.log("Frst time from select units and its not Undef");
                }
            } else {
                if (unitFormation === undefined) {
                    //console.log("Second time from select units and its UNDEFINED");
                    this.hideIcons(this.unitIcons, false);
                } else {
                    unitFormation = unitFormation.concat(this.unitIcons);
                    //console.log("Second time from select units and its not Undef");
                    this.hideIcons(unitFormation, false);
                }
            }
        }

        public generateUnits(unitFormation: Array<Unit>): Array<Unit> {
            this.unitFormation = unitFormation;

            for (let i: number = 0; i < this.unitFormation.length; i++) {
                //console.log("MSG1");
                //console.log(unitFormation);
                //console.log("MSG2");
                //console.log(this.unitFormation);
                if (this.unitFormation[i] === null) {
                    this.unitFormation.splice(i, 1, null);
                    //console.log("Null" + " at " + i.toString());
                } else if (this.unitFormation[i].name === GOLD_KNIGHT_STR) {
                    this.unitFormation.splice(i, 1, (new Knight(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN)));
                    //console.log(this.unitFormation[i].name + " at " + i.toString());
                } else if (this.unitFormation[i].name === ELF_MAGE_STR) {
                    this.unitFormation.splice(i, 1, (new Mage(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN)));
                    //console.log(this.unitFormation[i].name + " at " + i.toString());
                } else if (this.unitFormation[i].name === WOMAN_ARCHER_STR) {
                    this.unitFormation.splice(i, 1, (new Archer(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN)));
                    //console.log(this.unitFormation[i].name + " at " + i.toString());
                } else if (this.unitFormation[i].name === "Knight Spear") {
                    this.unitFormation.splice(i, 1, (new KnightSpear(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN)));
                    //console.log(this.unitFormation[i].name + " at " + i.toString());
                } else if (this.unitFormation[i].name === "Knight Axe") {
                    this.unitFormation.splice(i, 1, (new KnightAxe(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN)));
                    //console.log(this.unitFormation[i].name + " at " + i.toString());
                }

            }

            for (let index: number = 0; index < this.unitFormation.length; index++) {
                //console.log("MSG3");
                //console.log(unitFormation);
                //console.log("MSG4");
                //console.log(this.unitFormation);
                if (this.unitFormation[index] === null) {
                    //console.log("Null at set rect and generate units");
                } else {
                    const unit: Unit = this.unitFormation[index];
                    this.game.add.existing(unit.circleImage);
                    this.game.add.existing(unit.rectImage);
                    unit.circleImage.x = HIDE_FROM_SCREEN;
                    unit.rectImage.x = HIDE_FROM_SCREEN;
                }
            }

            this.alignRectImages(this.unitFormation);

            this.enableAndCheckUnits(this.unitFormation);

            // displays and updates unit info
            this.enableIconsTintAndInfo(this.unitFormation);

            return this.unitFormation;
        }

        private enableAndCheckUnits(unitFormation: Array<Unit>): void {
            unitFormation.forEach((unit) => {
                let tempPosX: number;
                let tempPosY: number;
                if (unit === null) {
                    //console.log(" Null at enable behavoiur");
                } else {
                    unit.circleImage.events.onDragStart.add(() => {
                        tempPosX = unit.circleImage.x;
                        tempPosY = unit.circleImage.y;
                        //console.log(tempPosX);
                    });
                    unit.circleImage.events.onDragStop.add(() => {
                        this.clickerSprites.forEach((clicker) => {
                            if ((unit.name === GOLD_KNIGHT_STR)) {
                                this.enableBehaviour(clicker, unit, tempPosX, tempPosY);
                            } else if ((unit.name === WOMAN_ARCHER_STR)) {
                                this.enableBehaviour(clicker, unit, tempPosX, tempPosY);
                            } else if ((unit.name === ELF_MAGE_STR)) {
                                this.enableBehaviour(clicker, unit, tempPosX, tempPosY);
                            } else if ((unit.name === "Knight Spear")) {
                                this.enableBehaviour(clicker, unit, tempPosX, tempPosY);
                            } else if ((unit.name === "Knight Axe")) {
                                this.enableBehaviour(clicker, unit, tempPosX, tempPosY);
                            }
                        });
                    });
                }
            });
        }

        private enableBehaviour(clicker: Phaser.Sprite, unit: Unit, tempX: number, tempY: number): void {
            if (this.checkOverlap(clicker, unit.circleImage)) {
                unit.rectImage.x = clicker.x;
                unit.rectImage.y = clicker.y;
                unit.circleImage.x = HIDE_FROM_SCREEN;
                //console.log("the circleimage x position is: " + unit.circleImage.x);
            }

            if (unit.circleImage.x === HIDE_FROM_SCREEN) {
                //console.log("position out of screen");
            } else if (unit.circleImage.x !== tempX || unit.circleImage.y !== tempX) {
                unit.circleImage.x = tempX;
                unit.circleImage.y = tempY;
                //console.log("the temp x position is: " + tempX);
                //console.log("the circleimage x position is: " + unit.circleImage.x);
            }

            // set unit icons for spending money
            if ((DrawUi.avaliableUserMoney >= 0) && (DrawUi.avaliableUserMoney >= unit.priceCost)) {
                if (this.checkOverlap(clicker, unit.circleImage)) {
                    //console.log("has money ");
                    DrawUi.avaliableUserMoney -= unit.priceCost;
                    DrawUi.userMoneyText.setText(GOLD_STR + DrawUi.avaliableUserMoney.toString());
                }
            }
            for (let index: number = 0; index < this.unitIcons.length; index++) {
                const unit: Unit = this.unitIcons[index];

                if (unit.priceCost > DrawUi.avaliableUserMoney) {
                    unit.circleImage.tint = COLOR_HOVER_RED_NUM2;
                    unit.circleImage.inputEnabled = false;
                    //console.log(unit.name + "cant buy nothing");
                }
            }
        }

        private checkOverlap(clicker: Phaser.Sprite, unit: Phaser.Sprite): boolean {
            let boundsClicker: any = clicker.getBounds();
            let boundsUnit: any = unit.getBounds();
            return Phaser.Rectangle.intersects(boundsClicker, boundsUnit);
        }

        private alignRectImages(unitFormation: Array<Unit>): void {
            for (let currentUnit: number = 0; currentUnit < unitFormation.length; currentUnit++) {
                const unit: Unit = unitFormation[currentUnit];
                let tempPosX: number;
                let tempPosY: number;
                if (unit === null) {
                    //console.log(" Null at enable clicker of current unit" + currentUnit.toString());
                } else {
                    unit.rectImage.events.onDragStart.add(() => {
                        tempPosX = unit.rectImage.x;
                        tempPosY = unit.rectImage.y;
                        //console.log(tempPosX);
                    });
                    unit.rectImage.events.onDragStop.add(() => {
                        //console.log("before unit rect image x" + unit.rectImage.x);
                        this.clickerSprites.forEach((clicker) => {
                            for (let checkedUnit: number = 0; checkedUnit < unitFormation.length; checkedUnit++) {
                                const unitCheck: Unit = unitFormation[checkedUnit];
                                if (unitCheck === null) {
                                    //console.log(" Null at enable checked unit");
                                } else {
                                    //console.log("unit x" + unit.rectImage.x + " unitCheck x" + unitCheck.rectImage.x);
                                    if (unit.rectImage.overlap(clicker)) {
                                        unit.rectImage.x = clicker.x;
                                        unit.rectImage.y = clicker.y;
                                        //console.log(" old behaviour");
                                    } else if (unit.name === unitCheck.name) {
                                        //console.log(" unit has the same name as unitcheck, so skip");
                                    } else if (unit.rectImage.overlap(unitCheck.rectImage)
                                        /*|| unit.rectImage.x !== tempPosX || unit.rectImage.y !== tempPosY*/) {
                                        //console.log(unit.name + " is overlaps over " + unitCheck.name);
                                        unit.rectImage.x = tempPosX;
                                        unit.rectImage.y = tempPosY;
                                    }
                                }
                            }
                        });
                    });
                }
            }
        }

        private enableIconsTintAndInfo(unitFormation: Array<Unit>): void {
            unitFormation.forEach((unit) => {
                if (unit === null) {
                    //console.log(" Null at unitinfo");
                } else {
                    unit.circleImage.events.onInputOver.add(() => {
                        unit.circleImage.tint = COLOR_HOVER_YELLOW_NUM;
                        if (unit.name === GOLD_KNIGHT_STR) {
                            this.unitInfoText.setText(unit.showUnitInfo());
                        } else if (unit.name === ELF_MAGE_STR) {
                            this.unitInfoText.setText(unit.showUnitInfo());
                        } else if (unit.name === WOMAN_ARCHER_STR) {
                            this.unitInfoText.setText(unit.showUnitInfo());
                        } else if (unit.name === "Knight Spear") {
                            this.unitInfoText.setText(unit.showUnitInfo());
                        } else if (unit.name === "Knight Axe") {
                            this.unitInfoText.setText(unit.showUnitInfo());
                        }
                    });
                    //console.log(unit.name);
                    unit.circleImage.events.onInputOut.add(() => {
                        unit.circleImage.tint = COLOR_CLEAR_NUM;
                        this.unitInfoText.setText("");
                    });
                    /**** for rect images too ***/
                    unit.rectImage.events.onInputOver.add(() => {
                        unit.rectImage.tint = COLOR_HOVER_YELLOW_NUM;
                        if (unit.name === GOLD_KNIGHT_STR) {
                            this.unitInfoText.setText(unit.showUnitInfo());
                        } else if (unit.name === ELF_MAGE_STR) {
                            this.unitInfoText.setText(unit.showUnitInfo());
                        } else if (unit.name === WOMAN_ARCHER_STR) {
                            this.unitInfoText.setText(unit.showUnitInfo());
                        } else if (unit.name === "Knight Spear") {
                            this.unitInfoText.setText(unit.showUnitInfo());
                        } else if (unit.name === "Knight Axe") {
                            this.unitInfoText.setText(unit.showUnitInfo());
                        }
                    });
                    //console.log(unit.name);
                    unit.rectImage.events.onInputOut.add(() => {
                        unit.rectImage.tint = COLOR_CLEAR_NUM;
                        this.unitInfoText.setText("");
                    });
                }
            });
        }

        private drawAllIconsAndText(unitFormation: Array<Unit>): void {
            const UNIT_TEXT_POS_X: number = 540;
            const UNIT_TEXT_POS_Y: number = 100;

            const MONEY_TEXT_POS_X: number = 10;
            const MONEY_TEXT_POS_Y: number = 700;

            //this.resetUnitsIcon = this.game.add.sprite(0, 0, "endTurnIcon");
            //this.resetUnitsIcon.alpha = 0;
            //this.resetUnitsIcon.inputEnabled = true;
            //this.resetUnitsIcon.events.onInputOver.add(() => {
            //    this.resetUnitsIcon.tint = COLOR_HOVER_YELLOW_NUM;
            //});
            //this.resetUnitsIcon.events.onInputOut.add(() => {
            //    this.resetUnitsIcon.tint = COLOR_CLEAR_NUM;
            //});
            //this.resetUnitsIcon.events.onInputDown.add(() => {
            //    this.resetUnitIconPositions(unitFormation);
            //});

            //this.slotsAvaliableText = this.game.add.text(TEXT_POS_X, TEXT_POS_Y,
            //    EMPTY_SLOTS.toString(), { font: FONT_ACME, fill: COLOR_YELLOW });
            //this.slotsAvaliableText.alpha = 0;

            this.unitInfoText = this.game.add.text(UNIT_TEXT_POS_X, UNIT_TEXT_POS_Y,
                "", { font: "22px Acme", fill: COLOR_ORANGE });
            this.unitInfoText.alpha = 0;

            DrawUi.userMoneyText = this.game.add.text(MONEY_TEXT_POS_X, MONEY_TEXT_POS_Y,
                GOLD_STR + DrawUi.avaliableUserMoney.toString(), { font: "15px Acme", fill: COLOR_GOLD });
            //DrawUi.userMoneyText.alpha = 1;
        }

        private loadClickers(): void {
            for (let i: number = 0; i < TOTAL_UNIT_SLOTS; i++) {
                this.clickerSprites.push(new Phaser.Sprite(this.game, 0, 0, "clicker"));
                this.clickerSprites[i].name = i.toString();
            }

            for (let index: number = 0; index < this.clickerSprites.length; index++) {
                const clicker: Phaser.Sprite = this.clickerSprites[index];
                this.game.add.existing(clicker);
                clicker.x = HIDE_FROM_SCREEN;
                clicker.alpha = 0;
            }

            //this.emptySlots = EMPTY_SLOTS;
            //this.slotsAvaliableText.setText((this.emptySlots).toString());
        }

        private loadDefaultIcons(): void {
            this.unitIcons.push(new Knight(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN));
            this.unitIcons.push(new Knight(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN));
            this.unitIcons.push(new Knight(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN));
            this.unitIcons.push(new Knight(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN));
            this.unitIcons.push(new Knight(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN));
            this.unitIcons.push(new Knight(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN));

            this.unitIcons.push(new Mage(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN));
            this.unitIcons.push(new Mage(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN));
            this.unitIcons.push(new Mage(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN));
            this.unitIcons.push(new Mage(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN));
            this.unitIcons.push(new Mage(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN));
            this.unitIcons.push(new Mage(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN));

            this.unitIcons.push(new Archer(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN));
            this.unitIcons.push(new Archer(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN));
            this.unitIcons.push(new Archer(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN));
            this.unitIcons.push(new Archer(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN));
            this.unitIcons.push(new Archer(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN));
            this.unitIcons.push(new Archer(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN));

            this.unitIcons.push(new KnightSpear(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN));
            this.unitIcons.push(new KnightSpear(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN));
            this.unitIcons.push(new KnightSpear(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN));
            this.unitIcons.push(new KnightSpear(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN));
            this.unitIcons.push(new KnightSpear(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN));
            this.unitIcons.push(new KnightSpear(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN));

            this.unitIcons.push(new KnightAxe(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN));
            this.unitIcons.push(new KnightAxe(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN));
            this.unitIcons.push(new KnightAxe(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN));
            this.unitIcons.push(new KnightAxe(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN));
            this.unitIcons.push(new KnightAxe(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN));
            this.unitIcons.push(new KnightAxe(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN));

            for (let index: number = 0; index < this.unitIcons.length; index++) {
                const unit: Unit = this.unitIcons[index];
                this.game.add.existing(unit.circleImage);
                this.game.add.existing(unit.rectImage);
                unit.circleImage.x = HIDE_FROM_SCREEN;
                unit.rectImage.x = HIDE_FROM_SCREEN;
            }

            this.alignRectImages(this.unitIcons);

            this.enableAndCheckUnits(this.unitIcons);

            // displays and updates unit info
            this.enableIconsTintAndInfo(this.unitIcons);
        }

        private allignAllCircleIcons(units: Array<Unit>): void {
            const CENTER_COL: number = 45;
            const POSITION_1: number = -33;
            const POSITION_2: number = -123;
            const POSITION_3: number = -213;
            const POSITION_4: number = -298;
            const POSITION_5: number = -386;

            for (var i: number = 0; i < units.length; i++) {
                if (units[i] === null) {
                    //console.log("Null" + " at allign all icons" + i.toString());
                    continue;
                } else {
                    //console.log(i + "   " + units[i].name);
                    if (units[i].name === GOLD_KNIGHT_STR) {
                        units[i].circleImage.alignIn(this.unitSelectionSprite, Phaser.TOP_CENTER, CENTER_COL, POSITION_1);
                    } else if (units[i].name === ELF_MAGE_STR) {
                        units[i].circleImage.alignIn(this.unitSelectionSprite, Phaser.TOP_CENTER, CENTER_COL, POSITION_2);
                    } else if (units[i].name === WOMAN_ARCHER_STR) {
                        units[i].circleImage.alignIn(this.unitSelectionSprite, Phaser.TOP_CENTER, CENTER_COL, POSITION_3);
                    } else if (units[i].name === "Knight Spear") {
                        units[i].circleImage.alignIn(this.unitSelectionSprite, Phaser.TOP_CENTER, CENTER_COL, POSITION_4);
                    } else if (units[i].name === "Knight Axe") {
                        units[i].circleImage.alignIn(this.unitSelectionSprite, Phaser.TOP_CENTER, CENTER_COL, POSITION_5);
                    }
                }
            }
        }

        private allignAllClickers(clicker: Array<Phaser.Sprite>): void {
            const POSITION_OFFS_X_1: number = -86;
            const POSITION_OFFS_X_2: number = -246;
            const POSITION_OFFS_Y: number = 183;

            clicker[0].alignIn(this.unitSelectionSprite, Phaser.CENTER, POSITION_OFFS_X_1, -POSITION_OFFS_Y);
            clicker[1].alignIn(this.unitSelectionSprite, Phaser.CENTER, POSITION_OFFS_X_1);
            clicker[2].alignIn(this.unitSelectionSprite, Phaser.CENTER, POSITION_OFFS_X_1, POSITION_OFFS_Y);

            clicker[3].alignIn(this.unitSelectionSprite, Phaser.CENTER, POSITION_OFFS_X_2, -POSITION_OFFS_Y);
            clicker[4].alignIn(this.unitSelectionSprite, Phaser.CENTER, POSITION_OFFS_X_2);
            clicker[5].alignIn(this.unitSelectionSprite, Phaser.CENTER, POSITION_OFFS_X_2, POSITION_OFFS_Y);
        }

        private hideAllClickers(clicker: Array<Phaser.Sprite>): void {
            clicker.forEach((clicker) => {
                clicker.x = HIDE_FROM_SCREEN;
            });
        }

        protected showIcons(unitFormation: Array<Unit>, concat: boolean = null): void {
            //const RESET_UNITS_ICON_X: number = this.unitSelectionSprite.width - 200;
            //const RESET_UNITS_ICON_Y: number = this.unitSelectionSprite.height - 130;

            this.unitSelectionSprite.x = UNIT_SELECTION_POS_X;
            //this.resetUnitsIcon.x = RESET_UNITS_ICON_X;
            //this.resetUnitsIcon.y = RESET_UNITS_ICON_Y;

            //if (this.emptySlots === EMPTY_SLOTS) {
            this.allignAllCircleIcons(unitFormation);
            //}
            this.allignAllClickers(this.clickerSprites);

            this.alignRectImages(unitFormation);

            // tween objects to visible
            this.game.add.tween(DrawUi.userMoneyText).to({ alpha: 1 }, 250, Phaser.Easing.Default, true);
            this.game.add.tween(this.unitInfoText).to({ alpha: 1 }, 250, Phaser.Easing.Default, true);
            this.game.add.tween(this.unitSelectionSprite).to({ alpha: 1 }, 250, Phaser.Easing.Default, true);
            //this.game.add.tween(this.slotsAvaliableText).to({ alpha: 1 }, 250, Phaser.Easing.Default, true);
            //this.game.add.tween(this.resetUnitsIcon).to({ alpha: 1 }, 250, Phaser.Easing.Default, true);

            for (var i: number = 0; i < unitFormation.length; i++) {
                if (unitFormation[i] === null) {
                    //console.log("Null" + "at show icons at " + i.toString());
                } else {
                    if (i < TOTAL_UNIT_SLOTS && concat) { // 6 units
                        //this.emptySlots--;
                        //this.slotsAvaliableText.setText((this.emptySlots).toString());
                        unitFormation[i].circleImage.x = HIDE_FROM_SCREEN;
                        unitFormation[i].rectImage.x = this.clickerSprites[i].x;
                        unitFormation[i].rectImage.y = this.clickerSprites[i].y;
                        //console.log("unit number " + i + " " + unitFormation[i].name);
                        //console.log(unitFormation);
                    }
                    //console.log(i + "   " + unitIcons[i].name);
                    this.game.add.tween(unitFormation[i].circleImage).to({ alpha: 1 }, 250, Phaser.Easing.Default, true);
                    this.game.add.tween(unitFormation[i].rectImage).to({ alpha: 1 }, 250, Phaser.Easing.Default, true);
                    unitFormation[i].circleImage.inputEnabled = true;
                    unitFormation[i].circleImage.input.enableDrag(false, true, false, null, null, this.unitSelectionSprite);
                    unitFormation[i].rectImage.inputEnabled = true;
                    unitFormation[i].rectImage.input.enableDrag(false, true, false, null, null, this.unitSelectionSprite);
                }
            }
        }

        protected hideIcons(unitFormation: Array<Unit>, concat: boolean = null): void {
            this.game.add.tween(this.unitInfoText).to({ alpha: 0 }, 250, Phaser.Easing.Default, true)
                .onComplete.addOnce(() => {
                    this.unitSelectionSprite.x = HIDE_FROM_SCREEN;
                });
            this.game.add.tween(this.unitSelectionSprite).to({ alpha: 0 }, 250, Phaser.Easing.Default, true)
                .onComplete.addOnce(() => {
                    this.unitSelectionSprite.x = HIDE_FROM_SCREEN;
                });

            for (let i: number = 0; i < unitFormation.length; i++) {
                if (unitFormation[i] === null) {
                    //console.log("Null" + "at show icons at " + i.toString());
                    continue;
                } else {
                    //console.log("begin tween" + "at show icons at " + i.toString());
                    unitFormation[i].circleImage.inputEnabled = false;
                    unitFormation[i].rectImage.inputEnabled = false;
                    this.game.add.tween(unitFormation[i].circleImage).to({ alpha: 0 }, 250,
                        Phaser.Easing.Default, true);
                    this.game.add.tween(unitFormation[i].rectImage).to({ alpha: 0 }, 250,
                        Phaser.Easing.Default, true);
                }
            }

            this.exportArray(unitFormation, concat);

            this.hideAllClickers(this.clickerSprites);
        }

        // Push units names into array for send to next state
        // I just hardcoded this sh*t
        private exportArray(unitFormation: Array<Unit>, concat: boolean = null): void {
            //debugger;

            for (let currentClickerIndex: number = 0; currentClickerIndex < this.clickerSprites.length; currentClickerIndex++) {
                const clicker: Phaser.Sprite = this.clickerSprites[currentClickerIndex];

                //console.log("I'm at export array at current clicker index of: " + currentClickerIndex.toString());
                if (!concat) {
                    //console.log("SPLICED! at currentClickerIndex with null, is finished:" + concat);
                    this.unitFormation.splice(currentClickerIndex, 1, null);
                }
                //console.log("DBG IVR1, Given unitFormation:");
                //console.log(unitFormation);
                //console.log("DBG IVR2, Current this.unitFormation:");
                //console.log(this.unitFormation);

                for (let currentUnitIndex: number = 0; currentUnitIndex < unitFormation.length; currentUnitIndex++) {
                    if (unitFormation[currentUnitIndex] === null && currentUnitIndex < TOTAL_UNIT_SLOTS) {
                        //console.log("Null so no export");
                    } else {
                        if (unitFormation[currentUnitIndex].rectImage.x === clicker.x &&
                            unitFormation[currentUnitIndex].rectImage.y === clicker.y) {
                            this.unitFormation.splice(currentClickerIndex, 1, unitFormation[currentUnitIndex]);
                            //console.log("unitFormation[currentUnitIndex].rectImage matches the clicker!");
                            //console.log(clicker.name + " " + unitFormation[currentUnitIndex].name);
                        } else {
                            //console.log("unitFormation[currentUnitIndex] !== null: clicker name:"
                            //    + clicker.name + " clicker.x:" + clicker.x + " y:" + clicker.y);
                        }
                    }
                }
            }
            //console.log("The final exported array is this.unitFormation: ");
            //console.log(this.unitFormation);
        }
    }
}
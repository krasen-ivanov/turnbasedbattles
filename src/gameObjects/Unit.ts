﻿module TurnBasedBattles {
    export enum UnitStates { IDLE, WALKLEFT, WALKRIGHT, ATTACK, HURT, DEAD }

    export class Unit extends Phaser.Sprite {

        game: Phaser.Game;
        animation: Phaser.Sprite;
        currentState: UnitStates;

        name: string;
        level: number;
        strength: number;
        agility: number;
        health: number;
        priceCost: number;
        priority: number;
        pointerA_x: number;
        pointerA_y: number;
        pointerB_x: number;
        pointerB_y: number;
        step_x: number;
        step_y: number;
        battleFieldHeight: number;
        canAttack: boolean;
        attackFinished: boolean;
        attackStarted: boolean;
        isEnemy: boolean;
        isHighlighted: boolean;
        isAttackable: boolean;
        isAttacked: boolean;
        isAlive: boolean;
        isWizard: boolean;
        isMelee: boolean;
        isOrc: boolean;
        isDone: boolean;
        continue: boolean;
        damageDone: boolean;
        initialPositionX: number;
        initialPositionY: number;
        defaultHealth: number;
        defaultXanchor: number;
        lvl: number;
        index: number;
        unitScale: number;

        rectImage: Phaser.Sprite;
        circleImage: Phaser.Sprite;
        HPbarTop: Phaser.Sprite;
        HPbarBottom: Phaser.Sprite;
        attackSignal: Phaser.Signal;
        showDMG: Phaser.Sprite;

        constructor(game: Phaser.Game, x: number, y: number) {
            super(game, 0, 0);
            this.game = game;
            this.isEnemy = false;
            this.isHighlighted = false;
            this.isAttackable = false;
            this.isAttacked = false;
            this.isAlive = true;
            this.isWizard = false;
            this.isMelee = false;
            this.isOrc = false;
            this.isDone = false;
            this.canAttack = false;
            this.attackFinished = true;
            this.attackStarted = false;
            this.continue = false;
            this.damageDone = false;
            this.initialPositionX = x;
            this.initialPositionY = y;
            this.lvl = 0;

            this.HPbarTop = this.game.make.sprite(x, y, "hpTop");
            this.HPbarBottom = this.game.make.sprite(x, y, "hpBottom");
            this.attackSignal = new Phaser.Signal();

        }

        public estimateHealthScale(): number {
            return (((this.health / this.defaultHealth) * SIZE_HP) > 0) ? ((this.health / this.defaultHealth) * SIZE_HP) : 0;
        }

        public ShowDamage(showDMG: Phaser.Sprite, Unit: Unit, damage: string): void {
            showDMG = this.game.add.text(Unit.x + BUFFER, Unit.y, damage, {
                font: "Arial Black",
                fontWeight: "bold",
                fontSize: SIZE_DMG,
                fill: "#ff2222",
                align: "center",
                stroke: "#000000",
                strokeThickness: 3
            });
            showDMG.anchor.setTo(0, 0.5);
            this.game.add.tween(showDMG).to({ y: Unit.y - 120 }, 1100, Phaser.Easing.Elastic.Out, true)
                .onComplete.add(() => {
                    showDMG.destroy();
                });
        }

        public TakeDamage(damage: number): void {
            this.isAttacked = true;
            this.health -= damage;
//console.log(this.health, this.defaultHealth);
            this.HPbarTop.scale.setTo(this.estimateHealthScale(), SIZE_HP - 0.02);
            //console.log(this.name, this.health);
        }

        public DoUnitDamage(Unit: Unit, damage: number): void {
            if (Unit != null && Unit.isAlive && (Unit.isAttackable || this.isWizard)) {
                Unit.TakeDamage(damage);
                Unit.isAttackable = false;
                this.ShowDamage(this.showDMG, Unit, "-" + damage.toString());
                //console.log("Do damage: " + this.name + " Take damage:" + Unit.name);
                this.damageDone = true;
            }
        }

        public DoDamage(unit1: Unit, unit2: Unit, unit3: Unit, unit4: Unit, unit5: Unit, unit6: Unit, damage: number): void {
            this.DoUnitDamage(unit1, damage);
            this.DoUnitDamage(unit2, damage);
            this.DoUnitDamage(unit3, damage);
            this.DoUnitDamage(unit4, damage);
            this.DoUnitDamage(unit5, damage);
            this.DoUnitDamage(unit6, damage);
            if (!this.damageDone) {
                this.ShowDamage(this.showDMG, this, "Miss");
            }
            this.isDone = true;
            this.damageDone = false;
        }

        protected highlightUnit(): void {
            this.animation.inputEnabled = true;
            this.animation.input.pixelPerfectOver = true;
            this.animation.input.pixelPerfectClick = true;
            this.animation.events.onInputOver.add(() => {
                this.animation.tint = COLOR_HOVER_RED_NUM2;
                //this.animation.input.useHandCursor = true;
                this.isHighlighted = true;
                //console.log("Highlighted");
            });
            this.animation.events.onInputOut.add(() => {
                this.animation.tint = COLOR_CLEAR_NUM;
                this.isHighlighted = false;
            });
        }

        protected highlightUnit2(): void {
            if (this.canAttack && !this.attackStarted) {
                this.animation.tint = COLOR_HOVER_GREEN_NUM3;
            } else {
                this.animation.tint = COLOR_CLEAR_NUM;
            }
        }

        public showUnitInfo(): string {
            return " " + this.name + "\n Level " + this.level + "\n STR " + this.strength +
                "\n AGI " + this.agility + "\n Health " + this.health + "\n Cost " + this.priceCost;
        }
    }
}
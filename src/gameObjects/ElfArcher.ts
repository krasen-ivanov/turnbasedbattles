/// <reference path = "../../src/gameObjects/EnemyRangeUnit.ts"/>

module TurnBasedBattles {

    export class ElfArcher extends EnemyRangeUnit {

        cursor: Phaser.CursorKeys;

        constructor(game: Phaser.Game, x: number, y: number, index: number = null) {
            super(game, x, y);
            this.game = game;
            this.currentState = UnitStates.IDLE;
            this.x = x;
            this.y = y;

            this.name = "Elf Archer";
            this.level = 3;
            this.strength = 40;
            this.agility = 9;
            this.health = 60;
            this.defaultHealth = this.health;
            this.defaultXanchor = 1;
            this.priceCost = 90;
            this.index = index;
            this.unitScale = SIZE_ELF_ARCHER;

            this.battleFieldHeight = MAP_HEIGHT / 2;
            //Add sprite
            this.circleImage = this.game.make.sprite(x, y, "elfArcher-circle");

            this.animation = this.game.add.sprite(this.x, this.y, "elfArcher");
            this.animation.animations.add("idle", [0, 1, 2, 3, 4], 10);
            this.animation.animations.add("walk", [5, 6, 7, 8, 9], 30);
            this.animation.animations.add("attack", [10, 11, 12, 13, 14], 20);
            this.animation.animations.add("hurt", [15, 16, 17, 18, 19], 20);
            this.animation.animations.add("die", [20, 21, 22, 23, 24], 15);
            this.animation.play("idle", 10, true);
            this.animation.anchor.setTo(this.defaultXanchor, 0.5);
            this.animation.scale.set(-this.unitScale, this.unitScale);
            this.animation.smoothed = false;

            this.highlightUnit();
        }

        protected AttackSequence(): void {
            super.AttackSequence();
        }

        public update(): void {
            super.update();
        }
    }
}
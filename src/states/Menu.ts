module TurnBasedBattles {

    export class Menu extends Phaser.State {

        private ui: DrawUi;

        public init(uiClass: DrawUi): void {
            this.ui = uiClass;
        }

        public create(): void {
            let backgroundImage: Phaser.Image = this.game.add.sprite(0, 0, "menuBackground");

            this.menuGameTitle();
            this.ui.drawMenuStateUI();
        }

        private menuGameTitle(): void {
            let titleBanner: Phaser.Image = this.game.add.sprite(433, 65, "titleBanner");
            titleBanner.scale.setTo(0.5, 0.5);
            titleBanner.width = 940;

            let gameTitle: Phaser.Text = this.game.add.text(630, 80, "Protectors of Goldcrest",
                {
                    font: FONT_BERKSHIRE,
                    fontWeight: "bold",
                    fontSize: 55,
                    fill: COLOR_ORANGE,
                    stroke: "#000000",
                    strokeThickness: 3
                });
        }
    }
}
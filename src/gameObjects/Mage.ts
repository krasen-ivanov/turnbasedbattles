module TurnBasedBattles {

    //export enum UnitStates { IDLE, WALKLEFT, WALKRIGHT, ATTACK, DEAD }

    export class Mage extends PlayerRangeUnit {

        //currentState: UnitStates;
        cursor: Phaser.CursorKeys;

        constructor(game: Phaser.Game, x: number, y: number, index: number = null) {
            super(game, x, y);
            this.game = game;
            this.currentState = UnitStates.IDLE;
            this.x = x;
            this.y = y;

            this.name = "Elf Mage";
            this.level = 2;
            this.strength = 35;
            this.agility = 4;
            this.health = 55;
            this.defaultHealth = this.health;
            this.defaultXanchor = 0;
            this.priceCost = 75;
            this.index = index;
            this.isWizard = true;
            this.unitScale = SIZE_MAGE;

            this.battleFieldHeight = MAP_HEIGHT / 2;
            //Add sprite
            this.rectImage = this.game.make.sprite(x, y, "elfMage-rect");
            this.circleImage = this.game.make.sprite(x, y, "elfMage-circle");

            this.animation = this.game.add.sprite(this.x, this.y, "elfMage");
            this.animation.animations.add("idle", [0, 1, 2, 3, 4], 30);
            this.animation.animations.add("walk", [5, 6, 7, 8, 9], 30);
            this.animation.animations.add("hurt", [25, 26, 27, 28, 29], 20);
            this.animation.animations.add("attack", [20, 21, 22, 23, 24], 10);
            this.animation.animations.add("die", [30, 31, 32, 33, 34], 15);
            this.animation.play("idle", 10, true);
            this.animation.anchor.setTo(this.defaultXanchor, 0.5);
            this.animation.scale.set(this.unitScale);
            this.animation.smoothed = false;

            this.highlightUnit();
        }

        protected AttackSequence(): void {
            super.AttackSequence();
        }

        public update(): void {
            super.update();
        }
    }
}
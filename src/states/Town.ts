module TurnBasedBattles {

    export class Town extends Phaser.State {

        private ui: DrawUi;
        private barracks: Phaser.Sprite;
        private armory: Phaser.Sprite;
        private questTower: Phaser.Sprite;
        private unitFormation: Array<Unit>;
        private intro: boolean = true;
        private tutorialsEnabled: boolean;
        //private playerDead: boolean;

        public init(uiClass: DrawUi, selectedUnitFormation: Array<Unit>, tutorialsEnabled: boolean): void {
            this.ui = uiClass;
            this.unitFormation = selectedUnitFormation;
            this.tutorialsEnabled = tutorialsEnabled;
            //console.log(this.tutorialsEnabled);
            //console.log(this.unitFormation);
            //this.playerDead = playerDead;

            //console.log("The selected units from MAP: ");
            //console.log(this.unitFormation);
        }

        public create(): void {
            this.loadTown();
            this.ui.drawTownStateUI(this.unitFormation);
            this.ui.townMusic();
            this.game.canvas.style.cursor = "url('src/assets/battlefield/icons/cursor2.png'), pointer";
            //console.log("TOWN");
        }

        private openCharacterMenu(open: boolean): void {
            //console.log("Clicked on museum");
            this.ui.isUnitSelectionOpen(open, this.unitFormation);
            this.armory.inputEnabled = false;
            this.questTower.inputEnabled = false;
        }

        private loadTown(): void {
            let backgroundTown: Phaser.Sprite = this.game.add.sprite(0, 0, "town");

            this.loadBarracks();
            this.loadArmory();
            this.loadQuestTower();
            if (this.tutorialsEnabled) {
                this.loadTutorialText();
            }
        }

        private loadTutorialText(): void {
            if ((this.unitFormation === undefined)) {
                let introSprite: Phaser.Image = this.game.add.sprite(300, 75, "bg-big");
                let introText: Phaser.Text = this.game.add.text(this.game.width / 2, this.game.height / 2,
                    "\nWelcome, Sire! \n\n Thank you for coming here to defend this town \n" +
                    "from the oncoming waves of enemies.\n"
                    + "\n They are very close. \n" +
                    " \n I suggest that you go to the barracks\n to recruit a small army\n" +
                    " and hunt down the enemies!\n\n<OK>", {
                        font: "27px Acme",
                        fill: "#281905",
                        align: "center"
                    });
                introText.alpha = 0;

                this.game.add.tween(introSprite).from({ height: 0 }, 1000, Phaser.Easing.Default, true, 0, 0, false)
                    .onComplete.addOnce(() => {

                        introText.anchor.setTo(0.5);
                        introText.alignIn(introSprite, Phaser.CENTER);
                        this.game.add.tween(introText).to({ alpha: 1 }, 500, Phaser.Easing.Default, true, 0, 0, false);

                        introText.inputEnabled = true;
                        introText.events.onInputDown.addOnce(() => {
                            introText.inputEnabled = false;
                            introSprite.destroy();
                            //bmd.clear();
                            introText.destroy();
                            //console.log("destroyed stuf");

                            //*****************part*two******************//

                            let secondSprite: Phaser.Image = this.game.add.sprite(this.game.width - 400, 100, "bg-small");
                            let secondText: Phaser.Text = this.game.add.text(this.game.width / 2, this.game.height / 2,
                                "\nAdd a few units to your army\nand set their position.\n" +
                                "Then you can go to the map\n to select a battle.\n\n < OK >", {
                                    font: "22px Acme",
                                    fill: "#281905",
                                    align: "center"
                                });
                            secondText.alpha = 0;

                            this.game.add.tween(secondSprite).from({ width: 0 }, 1000, Phaser.Easing.Default, true, 0, 0, false)
                                .onComplete.addOnce(() => {

                                    this.game.add.tween(secondText).to({ alpha: 1 }, 500, Phaser.Easing.Default, true, 0, 0, false);
                                    secondText.anchor.setTo(0.5);
                                    secondText.alignIn(secondSprite, Phaser.CENTER);

                                    secondText.inputEnabled = true;
                                    secondText.events.onInputDown.addOnce(() => {
                                        secondText.inputEnabled = false;
                                        secondSprite.destroy();
                                        //newBmd.clear();
                                        secondText.destroy();
                                        //console.log("destroyed stuf");
                                    });
                                });
                        });
                    });

            } else if ((this.unitFormation !== undefined) && this.intro) {
                //if (this.playerDead) {
                let upgradesSprite: Phaser.Image = this.game.add.sprite(10, 100, "bg-small");
                let upgradesText: Phaser.Text = this.game.add.text(this.game.width / 2, this.game.height / 2,
                    "\nWith the aquired gold\n from the fallen enemies, \nyou can purchase "
                    + "Upgrades\n for your army, Sire!\n\n < OK > ", {
                        font: "22px Acme",
                        fill: "#281905",
                        align: "center"
                    });
                upgradesText.alpha = 0;

                this.game.add.tween(upgradesSprite).from({ height: 0 }, 1000, Phaser.Easing.Default, true, 0, 0, false)
                    .onComplete.addOnce(() => {

                        this.game.add.tween(upgradesText).to({ alpha: 1 }, 500, Phaser.Easing.Default, true, 0, 0, false);
                        upgradesText.anchor.setTo(0.5);
                        upgradesText.alignIn(upgradesSprite, Phaser.CENTER);

                        upgradesText.inputEnabled = true;
                        upgradesText.events.onInputDown.addOnce(() => {
                            upgradesText.inputEnabled = false;
                            upgradesSprite.destroy();
                            //newBmd.clear();
                            upgradesText.destroy();
                            //console.log("destroyed null");
                            this.intro = false;
                        });
                    });

                //console.log("YOU WON, Coming from town");
                //} else {
                //    //console.log("You LOST, Coming from town dead first battle");
                //}
            } else if (!this.intro && (this.unitFormation !== undefined)) {
                //Don't display text
            }

            //if (!this.playerDead) {
            //    //console.log("You LOST, Coming from town");
            //}
        }

        private loadBarracks(): void {
            this.barracks = this.game.add.sprite(this.game.width, this.game.height, "museumR");
            this.barracks.anchor.set(1);
            this.barracks.inputEnabled = true;
            this.barracks.input.pixelPerfectOver = true;
            this.barracks.input.pixelPerfectClick = true;
            this.barracks.events.onInputOver.add(() => {
                this.barracks.tint = COLOR_HOVER_YELLOW_NUM;
                //this.barracks.input.useHandCursor = true;
            });
            this.barracks.events.onInputOut.add(() => {
                this.barracks.tint = COLOR_CLEAR_NUM;
            });

            let open: boolean = true;
            this.barracks.events.onInputDown.add(() => {
                if (open) {
                    this.openCharacterMenu(open);
                    open = false;
                } else {
                    this.openCharacterMenu(open);
                    open = true;
                    this.armory.inputEnabled = true;
                    this.questTower.inputEnabled = true;
                }
            });
        }

        private loadQuestTower(): void {
            const QUEST_TOWER_X_POS: number = 694;
            const QUEST_TOWER_Y_POS: number = 225;

            this.questTower = this.game.add.sprite(QUEST_TOWER_X_POS, QUEST_TOWER_Y_POS, "bellTowerM");
            this.questTower.inputEnabled = true;
            this.questTower.input.pixelPerfectOver = true;
            this.questTower.input.pixelPerfectClick = true;
            this.questTower.events.onInputOver.add(() => {
                this.questTower.tint = COLOR_HOVER_YELLOW_NUM;
                //this.questTower.input.useHandCursor = true;
            });
            this.questTower.events.onInputOut.add(() => {
                this.questTower.tint = COLOR_CLEAR_NUM;
            });
            this.questTower.events.onInputDown.add(() => {
                //console.log("clicked on bell");
                this.ui.bellSound();
                // TODO: add this...
            });
        }

        private loadArmory(): void {
            this.armory = this.game.add.sprite(0, this.game.height, "castleL");
            this.armory.anchor.set(0, 1);
            this.armory.inputEnabled = true;
            this.armory.input.pixelPerfectOver = true;
            this.armory.input.pixelPerfectClick = true;
            this.armory.events.onInputOver.add(() => {
                this.armory.tint = COLOR_HOVER_YELLOW_NUM;
                //this.armory.input.useHandCursor = true;
            });
            this.armory.events.onInputOut.add(() => {
                this.armory.tint = COLOR_CLEAR_NUM;
            });

            let isOpen: boolean = true;
            this.armory.events.onInputDown.add(() => {
                if (isOpen) {
                    this.openArmoryMenu(isOpen);
                    isOpen = false;
                } else {
                    this.openArmoryMenu(isOpen);
                    isOpen = true;
                    this.barracks.inputEnabled = true;
                    this.questTower.inputEnabled = true;
                }
            });
        }

        private openArmoryMenu(isOpen: boolean): void {
            //console.log("Clicked on armory");
            //console.log(isOpen);
            this.ui.isUpgradeUnitsMenuOpen(isOpen, this.unitFormation);
            this.barracks.inputEnabled = false;
            this.questTower.inputEnabled = false;
        }
    }
}
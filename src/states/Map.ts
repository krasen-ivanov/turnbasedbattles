﻿/// <reference path = "../../lib/phaser.d.ts"/>
/// <reference path = "../UI/DrawUI.ts"/>

module TurnBasedBattles {

    const MAP_WIDTH: number = 1300;
    const MAP_HEIGHT: number = 750;

    export class Map extends Phaser.State {

        ui: DrawUi;
        unitFormation: Array<Unit>;

        backgroundMap: Phaser.Image;
        mapMissionOne: Phaser.Image;
        mapMissionTwo: Phaser.Image;
        mapMissionThree: Phaser.Image;
        mapMissionFour: Phaser.Image;
        mapMissionFive: Phaser.Image;
        mapMissionBoss: Phaser.Image;

        missionOneCompleted: boolean;
        missionTwoCompleted: boolean;
        missionThreeCompleted: boolean;
        missionFourCompleted: boolean;
        missionFiveCompleted: boolean;
        missionBossCompleted: boolean;

        background: string;

        init(uiClass: DrawUi, selectedUnitFormation: Array<Unit>, background: string, levelComplete: boolean): void {
            this.ui = uiClass;
            this.unitFormation = selectedUnitFormation;
            //console.log(this.ui.playMusic);

            //console.log("The selected units from previous state: ");
            //console.log(this.unitFormation);
            this.mapCompleted(background, levelComplete);
        }

        create(): void {
            this.backgroundMap = this.game.add.image(0, 0, "map-bg");
            this.backgroundMap.width = MAP_WIDTH;
            this.backgroundMap.height = MAP_HEIGHT;

            this.drawMissionIcons();
            this.ui.drawMapStateUI();

            //fix for when returning from battle the input is disabled
            //TODO: do it right
            this.game.input.enabled = true;
        }

        mapCompleted(background: string, isComplete: boolean): void {
            switch (background) {
                case "Forest": {
                    this.missionOneCompleted = true;
                    break;
                }
                case "Waterfall": {
                    this.missionTwoCompleted = true;
                    break;
                }
                case "Mountain": {
                    this.missionThreeCompleted = true;
                    break;
                }
                case "PineForest": {
                    this.missionFourCompleted = true;
                    break;
                }
                case "PineForest2": {
                    this.missionFiveCompleted = true;
                    break;
                }
                case "BossGround": {
                    this.missionBossCompleted = true;
                    break;
                }
                default: {
                    break;
                }
            }
        }

        loadMission(mission: Phaser.Image, x: number, y: number, background: string, isComplete: boolean): void {
            mission = this.game.add.sprite(x, y, "battleSpot");
            if (background === "BossGround") {
                mission.scale.set(1);
            } else {
                mission.scale.set(0.5);
            }
            mission.inputEnabled = true;
            mission.tint = isComplete ? COLOR_HOVER_GREEN_NUM3 : COLOR_HOVER_RED_NUM2;
            mission.events.onInputOver.add(() => {
                mission.tint = COLOR_HOVER_YELLOW_NUM;
            });
            mission.events.onInputOut.add(() => {
                mission.tint = isComplete ? COLOR_HOVER_GREEN_NUM3 : COLOR_HOVER_RED_NUM2;
            });
            mission.events.onInputDown.add(() => {
                this.game.state.start(FIELD_BATTLE_STATE, true, false, this.ui, this.unitFormation, background, isComplete);
            });
        }

        drawMissionIcons(): void {

            this.loadMission(this.mapMissionOne, 900, 310, "Forest", this.missionOneCompleted);

            if (this.missionOneCompleted) {
                this.loadMission(this.mapMissionTwo, 1050, 420, "Waterfall", this.missionTwoCompleted);
            }
            if (this.missionTwoCompleted) {
                this.loadMission(this.mapMissionThree, 900, 550, "Mountain", this.missionThreeCompleted);
            }
            if (this.missionThreeCompleted) {
                this.loadMission(this.mapMissionFour, 300, 600, "PineForest", this.missionFourCompleted);
            }
            if (this.missionFourCompleted) {
                this.loadMission(this.mapMissionFive, 200, 300, "PineForest2", this.missionFiveCompleted);
            }
            if (this.missionFiveCompleted) {
                this.loadMission(this.mapMissionBoss, 600, 300, "BossGround", this.missionBossCompleted);
            }
        }
    }
}
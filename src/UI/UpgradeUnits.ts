﻿module TurnBasedBattles {
    const ARMORY_POS_X: number = 370;
    const ARMORY_POS_Y: number = 50;

    export class UpgradeUnits extends Phaser.Sprite {

        public game: Phaser.Game;

        private armorySprite: Phaser.Sprite;
        private sword1: Phaser.Image;
        private infoText: Phaser.Text;

        private sword2: Phaser.Image;
        private sword3: Phaser.Image;
        private equipmentArms: Phaser.Image;
        private armor1: Phaser.Image;
        private armor2: Phaser.Image;
        private armor3: Phaser.Image;
        private equipmentLegs: Phaser.Image;
        //private unitFormation: Array<Unit>;

        constructor(game: Phaser.Game, x: number, y: number, ) {
            super(game, x, y);
            this.game = game;
            //console.log("Opened the upgrades menu");

            this.armorySprite = this.game.add.sprite(ARMORY_POS_X, ARMORY_POS_Y, "armory");
            this.armorySprite.alpha = 0;
            this.armorySprite.x = HIDE_FROM_SCREEN;

            this.infoText = this.game.add.text(0, 0, "", {
                font: "22px Acme",
                fill: COLOR_ORANGE,
                align: "center"
            });

            //this.unitFormation = unitFormation;

            this.sword1 = this.game.add.sprite(HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, "sword1");
            this.sword2 = this.game.add.sprite(HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, "sword2");
            this.sword3 = this.game.add.sprite(HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, "sword3");
            this.equipmentArms = this.game.add.sprite(HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, "equipment-arms");
            this.armor1 = this.game.add.sprite(HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, "armor1");
            this.armor2 = this.game.add.sprite(HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, "armor2");
            this.armor3 = this.game.add.sprite(HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, "armor3");
            this.equipmentLegs = this.game.add.sprite(HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, "equipment-legs");
        }

        public displayUpgradeUnitsMenu(isOpen: boolean, unitFormation: Array<Unit>): void {
            if (isOpen) {
                this.armorySprite.x = ARMORY_POS_X;
                this.game.add.tween(this.armorySprite).to({ alpha: 1 }, 250, Phaser.Easing.Default, true);
                this.showUpgrades(unitFormation);
            } else {
                this.game.add.tween(this.armorySprite).to({ alpha: 0 }, 250, Phaser.Easing.Default, true)
                    .onComplete.addOnce(() => {
                        this.armorySprite.x = HIDE_FROM_SCREEN;
                    });
                this.hideUpgrades();
            }
        }

        private showUpgrades(unitFormation: Array<Unit>): void {
            //console.log("Show upgrade icons");
            this.loadSwords(unitFormation);
            this.loadArmor(unitFormation);
            this.loadEquipment(unitFormation);
        }

        private hideUpgrades(): void {
            this.game.add.tween(this.infoText).to({ alpha: 0 }, 250, Phaser.Easing.Default, true);

            this.game.add.tween(this.sword1).to({ alpha: 0 }, 250, Phaser.Easing.Default, true);
            this.game.add.tween(this.sword2).to({ alpha: 0 }, 250, Phaser.Easing.Default, true);
            this.game.add.tween(this.sword3).to({ alpha: 0 }, 250, Phaser.Easing.Default, true);
            this.game.add.tween(this.equipmentArms).to({ alpha: 0 }, 250, Phaser.Easing.Default, true);

            this.game.add.tween(this.armor1).to({ alpha: 0 }, 250, Phaser.Easing.Default, true);
            this.game.add.tween(this.armor2).to({ alpha: 0 }, 250, Phaser.Easing.Default, true);
            this.game.add.tween(this.armor3).to({ alpha: 0 }, 250, Phaser.Easing.Default, true);
            this.game.add.tween(this.equipmentLegs).to({ alpha: 0 }, 250, Phaser.Easing.Default, true);
        }

        private loadSwords(unitFormation: Array<Unit>): void {
            this.loadUpgrade(this.sword1, Phaser.LEFT_TOP, -96, -58,
                100, "Increases the strength\n of your units with +10 STR\n Costs 100 Gold", unitFormation);
            this.loadUpgrade(this.sword2, Phaser.LEFT_TOP, -96 - this.sword1.width, -58,
                250, "Increases the strength\n of your units with aditional +35 STR\n Costs 250 Gold", unitFormation);
            this.loadUpgrade(this.sword3, Phaser.LEFT_TOP, -96 - this.sword1.width - this.sword2.width, -58,
                420, "Increase the strength\n of your units with +50 STR\n Costs 420 Gold", unitFormation);
        }

        private loadArmor(unitFormation: Array<Unit>): void {
            this.loadUpgrade(this.armor1, Phaser.LEFT_CENTER, -96, -16,
                100, "Increase the health\n of your units with +100 Health\n Costs 100 Gold", unitFormation);
            this.loadUpgrade(this.armor2, Phaser.LEFT_CENTER, -96 - this.armor1.width, -16,
                200, "Increase the health\n of your units with +125 additional Health\n Costs 200 Gold", unitFormation);
            this.loadUpgrade(this.armor3, Phaser.LEFT_CENTER, -96 - this.armor1.width - this.armor2.width, -16,
                300, "Increase the health\n of your units with +150 additional Health\n Costs 300 Gold", unitFormation);
        }

        private loadEquipment(unitFormation: Array<Unit>): void {
            this.loadUpgrade(this.equipmentArms, Phaser.LEFT_TOP, -96 - this.sword1.width
                - this.sword2.width - this.sword3.width, -58, 200,
                "Increase the agility\n of your units with +15 AGI\n Costs 200 Gold", unitFormation);
            this.loadUpgrade(this.equipmentLegs, Phaser.LEFT_CENTER, -96 - this.armor1.width
                - this.armor2.width - this.armor3.width, -16, 300,
                "Increase the agility\n of your units with +30 AGI\n Costs 300 Gold", unitFormation);
        }

        private loadUpgrade(upgrade: Phaser.Image, pos: number, offX: number, offY: number, cost: number,
            upgrText: string, unitFormation: Array<Unit>): void {
            upgrade.alignIn(this.armorySprite, pos, offX, offY);
            this.game.add.tween(upgrade).to({ alpha: 1 }, 250, Phaser.Easing.Default, true);
            upgrade.inputEnabled = true;
            upgrade.events.onInputDown.addOnce(() => {
                if (upgrade.alive) {
                    if ((DrawUi.avaliableUserMoney >= 0) && (DrawUi.avaliableUserMoney >= cost)) {
                        {
                            DrawUi.avaliableUserMoney -= cost;
                            DrawUi.userMoneyText.setText(GOLD_STR + DrawUi.avaliableUserMoney.toString());
                            upgrade.alive = false;
                            //console.log("Upgraded!" + Knight.strength);
                            upgrade.tint = 0xbaadb9;
                            //Knight.strength += 200;
                            // //console.log("Upgraded!" + Knight.strength);
                            if (unitFormation === undefined) {
                                //console.log("Undefined unit formation!");
                            } else {
                                unitFormation.forEach((unit) => {
                                    if (unit === null) {
                                        //console.log("Null!");
                                    } else {
                                        unit.strength += 200;
                                        //console.log("Upgraded!" + unit.strength);
                                    }
                                });
                            }
                        }
                    }
                }
            });
            upgrade.events.onInputOver.add(() => {
                if (upgrade.alive) {
                    if ((DrawUi.avaliableUserMoney >= 0) && (DrawUi.avaliableUserMoney >= cost)) {
                        {
                            upgrade.tint = COLOR_HOVER_YELLOW_NUM;
                        }
                    } else {
                        upgrade.tint = COLOR_HOVER_RED_NUM2;
                    }
                }
                this.infoText.setText(upgrText);
                this.infoText.alignIn(this.armorySprite, Phaser.BOTTOM_CENTER, -10, -80);
                this.infoText.alpha = 1;
            });
            upgrade.events.onInputOut.add(() => {
                if (upgrade.alive) {
                    upgrade.tint = COLOR_CLEAR_NUM;
                }
                this.infoText.setText("");
                this.infoText.alpha = 0;
            });
        }
    }
}
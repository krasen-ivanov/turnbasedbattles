﻿module TurnBasedBattles {

    const UNIT_SELECTION_POS_X: number = 50;
    const UNIT_SELECTION_POS_Y: number = 50;

    const ARMORY_POS_X: number = 370;
    const ARMORY_POS_Y: number = 50;

    const TOP_BUTTON_COLOR: string = "#2d2e30";
    const TOP_BUTTON_COLOR_HOVER: string = "#181b21";
    const BOTTOM_BUTTON_COLOR: string = "#d1861d";
    const BOTTOM_BUTTON_COLOR_HOVER: string = "#f26321";
    const TUTORIALS_TEXT_COLOR: string = "#d11119";

    export class DrawUi extends Phaser.Sprite {

        public game: Phaser.Game;

        private nextIcon: Phaser.Image;
        private returnIcon: Phaser.Image;
        private endTurnIcon: Phaser.Image;

        private iconClick: Phaser.Sound;
        private townTheme: Phaser.Sound;
        private mapTheme: Phaser.Sound;
        private menuTheme: Phaser.Sound;
        private battleTheme: Phaser.Sound;
        private playMusic: boolean;

        private upgradeUnitsMenu: UpgradeUnits;
        private unitSelectionMenu: SelectUnits;
        private infoText: Phaser.Text;

        public static avaliableUserMoney: number;
        public static userMoneyText: Phaser.Text;
        private goldIcon: Phaser.Image;

        private battleUi: BattleUI;

        private startButton: Phaser.Image;
        private startButtonText: Phaser.Text;
        private optionsButton: Phaser.Image;
        private optionsButtonText: Phaser.Text;
        private exitButton: Phaser.Image;
        private exitButtonText: Phaser.Text;

        private isOpen: boolean;
        private speakerButton: Phaser.Image;
        private muteButton: Phaser.Image;
        private enableTutorialsText: Phaser.Text;
        private tutorialsEnabled: boolean;

        constructor(game: Phaser.Game, x: number, y: number, playMusic: boolean) {
            super(game, x, y);
            this.game = game;
            this.playMusic = playMusic;
            this.isOpen = false;
            this.tutorialsEnabled = true;
            DrawUi.avaliableUserMoney = START_MONEY;
        }

        public drawMenuStateUI(): void {
            this.game.canvas.style.cursor = "url('src/assets/battlefield/icons/cursor2.png'), pointer";

            this.createMenuStartButton();
            this.startButton.events.onInputDown.add(() => {
                if (this.playMusic) {
                    this.menuTheme.stop();
                    //this.townMusic();
                }
            });
            this.createMenuOptionsButton();
            this.createMenuSpeakerButton();
            this.createMenuMuteButton();
            this.createMenuExitButton();
            this.createMenuTutorialsButton();
        }

        public drawTownStateUI(unitFormation: Array<Unit>): void {
            //console.log(DrawUi.avaliableUserMoney);

            // TODO: this must not be here, because they are created every time
            this.unitSelectionMenu = new SelectUnits(this.game, 0, 0, unitFormation);
            this.upgradeUnitsMenu = new UpgradeUnits(this.game, 0, 0);

            this.goldIcon = this.game.add.sprite(85, 695, "gold");

            this.drawNextIcon();
            this.nextIcon.events.onInputDown.add(() => {
                this.checkForUnits();
            });
            this.drawReturnIcon();
            this.returnIcon.events.onInputDown.add(() => {
                this.game.state.start(MENU_STATE, true, false, this);
                if (this.playMusic) {
                    this.townTheme.stop();
                    this.menuMusic();
                }
            });
        }

        public drawMapStateUI(): void {
            this.game.canvas.style.cursor = "url('src/assets/battlefield/icons/cursor2.png'), pointer";
            this.drawReturnIcon();
            this.returnIcon.events.onInputDown.add(() => {
                this.clickIconSound();
                this.game.state.start(TOWN_STATE, true, false, this, this.unitSelectionMenu.unitFormation);
                if (this.playMusic) {
                    this.mapTheme.stop();
                    //this.townMusic();
                }
            });
        }

        public drawFieldBattleStateUI(): void {
            this.game.canvas.style.cursor = "url('src/assets/battlefield/icons/cursor.png'), pointer";
            // TODO: fix position of ui when the camera is moving
            this.stopCurrentMusic();
            this.clickIconSound();
            this.battleMusic();
            this.drawReturnIcon();
            this.returnIcon.events.onInputDown.add(() => {
                this.game.state.start(MAP_STATE, true, false, this, this.unitSelectionMenu.unitFormation);
                if (this.playMusic) {
                    this.battleTheme.stop();
                    this.mapMusic();
                }
            });

            // TODO: maybe fix this
            this.battleUi = new BattleUI(this.game, 0, 0);
        }

        public isUpgradeUnitsMenuOpen(isOpen: boolean, unitFormation: Array<Unit>): void {
            if (this.playMusic) {
                this.clickIconSound();
            }
            this.upgradeUnitsMenu.displayUpgradeUnitsMenu(isOpen, this.unitSelectionMenu.unitFormation);
        }

        public isUnitSelectionOpen(isOpen: boolean, unitFormation: Array<Unit>): void {
            if (this.playMusic) {
                this.clickIconSound();
            }
            this.unitSelectionMenu.displayUnitSelectionMenu(isOpen, unitFormation);
        }

        private drawReturnIcon(): void {
            this.returnIcon = this.game.add.sprite(0, 0, "returnIconO");
            this.returnIcon.x = this.game.width / 2 - this.returnIcon.width * 0.5;
            this.returnIcon.y = this.game.height - this.returnIcon.height;
            this.returnIcon.fixedToCamera = true;
            this.returnIcon.inputEnabled = true;
            this.returnIcon.events.onInputOver.add(() => {
                this.returnIcon.tint = COLOR_HOVER_YELLOW_NUM;
            });
            this.returnIcon.events.onInputOut.add(() => {
                this.returnIcon.tint = COLOR_CLEAR_NUM;
            });
            this.returnIcon.events.onInputDown.add(() => {
                if (this.playMusic) {
                    this.clickIconSound();
                }
            });
        }

        private drawNextIcon(): void {
            this.nextIcon = this.game.add.sprite(0, 0, "nextIconOr");
            this.nextIcon.x = this.game.width - this.nextIcon.width;
            this.nextIcon.y = this.game.height - this.nextIcon.height;
            this.nextIcon.inputEnabled = true;
            this.nextIcon.events.onInputOver.add(() => {
                this.nextIcon.tint = COLOR_HOVER_YELLOW_NUM;
            });
            this.nextIcon.events.onInputOut.add(() => {
                this.nextIcon.tint = COLOR_CLEAR_NUM;
            });
            this.nextIcon.events.onInputDown.add(() => {
                if (this.playMusic) {
                    this.clickIconSound();
                }
            });
        }

        private drawEndTurnIcon(): void {
            this.endTurnIcon = this.game.add.sprite(0, 0, "endTurnIcon");
            this.endTurnIcon.x = this.game.width - this.endTurnIcon.width;
            this.endTurnIcon.y = this.game.height - this.endTurnIcon.height;
            this.endTurnIcon.fixedToCamera = true;
            this.endTurnIcon.inputEnabled = true;
            this.endTurnIcon.events.onInputOver.add(() => {
                this.endTurnIcon.tint = COLOR_HOVER_YELLOW_NUM;
            });
            this.endTurnIcon.events.onInputOut.add(() => {
                this.endTurnIcon.tint = COLOR_CLEAR_NUM;
            });
            this.endTurnIcon.events.onInputDown.add(() => {
                if (this.playMusic) {
                    this.clickIconSound();
                }
            });
        }

        private checkForUnits(): void {
            //console.log("test1");
            let tempStop: boolean;
            for (var i: number = 0; i < this.unitSelectionMenu.unitFormation.length; i++) {
                if (this.unitSelectionMenu.unitFormation[i] === null) {
                    tempStop = false;
                } else {
                    tempStop = true;
                    // this means the UI so no more ui instances will be created in the next states
                    this.game.state.start(MAP_STATE, true, false, this, this.unitSelectionMenu.unitFormation);
                    //console.log("How many times");
                    if (this.playMusic) {
                        this.townTheme.stop();
                        this.mapMusic();
                    }
                }
            }
            if (!tempStop) {
                //console.log("test2");
                this.infoText = this.game.add.text(100, 15, "You need to buy units before you go to fight",
                    { font: FONT_ACME, fill: BOTTOM_BUTTON_COLOR });
                this.game.add.tween(this.infoText).to({ alpha: 0 }, 2500, Phaser.Easing.Default, true);
            }
        }
        /**
         * ******************** sounds
         */

        public attackSound(): void {
            if (this.playMusic) {
                let swordAttack: Phaser.Sound = this.game.add.audio("sword_attack");
                swordAttack.play();
            } else {
                this.game.sound.stopAll();
                //console.log("game_over sound is stopping all");
            }
        }
        public gameLostMusic(): void {
            if (this.playMusic) {
                let gameLose: Phaser.Sound = this.game.add.audio("game_over");
                gameLose.play();
            } else {
                this.game.sound.stopAll();
                //console.log("game_over sound is stopping all");
            }
        }
        public gameWinMusic(): void {
            if (this.playMusic) {
                let gameWin: Phaser.Sound = this.game.add.audio("gamewin");
                gameWin.play();
            } else {
                this.game.sound.stopAll();
                //console.log("gameWinMusic sound is stopping all");
            }
        }
        public bellSound(): void {
            if (this.playMusic) {
                let bell: Phaser.Sound = this.game.add.audio("bell");
                bell.play();
            } else {
                this.game.sound.stopAll();
                //console.log("clicking sound is stopping all");
            }
        }
        public stopMusic(): void {
            if (this.playMusic) {
                this.game.sound.stopAll();
                //console.log("stopMusic sound is stopping all");
            }
        }
        public clickIconSound(): void {
            if (this.playMusic) {
                this.iconClick = this.game.add.audio("buttonClick");
                this.iconClick.play();
            } else {
                this.game.sound.stopAll();
                //console.log("clicking sound is stopping all");
            }
        }
        private menuMusic(): void {
            if (this.playMusic) {
                this.menuTheme = this.game.add.audio("menuTheme");
                this.menuTheme.play();
            } else {
                this.game.sound.stopAll();
                //console.log("menu music is stopping all");
            }
        }
        public townMusic(): void {
            if (this.playMusic) {
                this.townTheme = this.game.add.audio("townTheme");
                //this.townTheme.volume = 0.5;
                this.townTheme.play(null, null, 0.5, true);
            } else {
                this.game.sound.stopAll();
                //console.log("town music is stopping all");
            }
        }
        private mapMusic(): void {
            if (this.playMusic) {
                this.mapTheme = this.game.add.audio("mapTheme");
                this.mapTheme.play();
            } else {
                this.game.sound.stopAll();
                //console.log("map music is stopping all");
            }
        }
        private battleMusic(): void {
            if (this.playMusic) {
                this.battleTheme = this.game.add.audio("battleTheme");
                this.battleTheme.play(null, null, 0.5, true);
            } else {
                this.game.sound.stopAll();
                //console.log("map music is stopping all");
            }
        }
        private stopCurrentMusic(): void {
            this.game.sound.stopAll();
        }

        /**
         * ***************** buttons
         */
        private createMenuStartButton(): void {
            this.startButton = this.game.add.sprite(100, 100, "menuButton1");
            this.startButton.scale.setTo(0.3, 0.2);
            this.startButton.inputEnabled = true;

            this.startButtonText = this.game.add.text(0, 0, "START", { font: FONT_ACME, fill: TOP_BUTTON_COLOR });
            this.startButtonText.anchor.set(-1.6, -0.3);
            this.startButtonText.position = this.startButton.position;

            this.startButton.events.onInputOver.add(() => {
                this.startButtonText.fill = BOTTOM_BUTTON_COLOR_HOVER;
            });
            this.startButton.events.onInputOut.add(() => {
                this.startButtonText.fill = TOP_BUTTON_COLOR;
            });
            this.startButton.events.onInputDown.add(() => {
                this.game.state.start(TOWN_STATE, true, false, this, undefined, this.tutorialsEnabled);
                if (this.playMusic) {
                    this.clickIconSound();
                    this.stopCurrentMusic();
                }
            });
        }

        private createMenuOptionsButton(): void {
            this.optionsButton = this.game.add.sprite(100, 200, "menuButton1");
            this.optionsButton.scale.setTo(0.3, 0.2);
            this.optionsButton.inputEnabled = true;

            this.optionsButtonText = this.game.add.text(0, 0, "    OPTIONS", { font: FONT_ACME, fill: TOP_BUTTON_COLOR });
            this.optionsButtonText.anchor.set(-0.7, -0.3);
            this.optionsButtonText.position = this.optionsButton.position;

            this.optionsButton.events.onInputOver.add(() => {
                this.optionsButtonText.fill = BOTTOM_BUTTON_COLOR_HOVER;
            });
            this.optionsButton.events.onInputOut.add(() => {
                this.optionsButtonText.fill = TOP_BUTTON_COLOR;
            });

            this.optionsButton.events.onInputDown.add(() => {
                if (this.isOpen) {
                    this.speakerButton.alpha = 1;
                    this.muteButton.alpha = 1;
                    this.enableTutorialsText.alpha = 1;
                    this.isOpen = false;
                } else {
                    this.speakerButton.alpha = 0;
                    this.muteButton.alpha = 0;
                    this.enableTutorialsText.alpha = 0;
                    this.isOpen = true;
                }
            });
        }

        private createMenuTutorialsButton(): void {
            this.enableTutorialsText = this.game.add.text(this.game.width * 0.22, this.game.height * 0.54,
                "Disable tutorials ? <OK>", {
                    font: "27px Acme",
                    fill: TUTORIALS_TEXT_COLOR,
                    align: "center"
                });
            this.enableTutorialsText.anchor.setTo(0.5);
            this.enableTutorialsText.alpha = 0;

            this.enableTutorialsText.inputEnabled = true;
            this.enableTutorialsText.events.onInputDown.addOnce(() => {
                this.enableTutorialsText.inputEnabled = false;
                this.enableTutorialsText.destroy();
                this.tutorialsEnabled = false;
            });
        }

        private createMenuSpeakerButton(): void {
            this.speakerButton = this.game.add.image(200, 280, "speaker");
            this.speakerButton.alpha = 0;
            this.speakerButton.inputEnabled = true;
            this.speakerButton.events.onInputDown.add(() => {
                if (!this.playMusic) {
                    this.playMusic = true;
                    this.menuMusic();
                }
                this.speakerButton.tint = COLOR_HOVER_GREEN_NUM3;
                this.muteButton.tint = COLOR_CLEAR_NUM;
            });
            this.speakerButton.events.onInputOut.add(() => {
                this.speakerButton.tint = COLOR_CLEAR_NUM;
            });
        }

        private createMenuMuteButton(): void {
            this.muteButton = this.game.add.image(280, 280, "mute");
            this.muteButton.alpha = 0;
            this.muteButton.inputEnabled = true;
            this.muteButton.events.onInputDown.add(() => {
                if (this.playMusic) {
                    this.clickIconSound();
                    this.playMusic = false;
                    this.game.sound.stopAll();
                }
                this.muteButton.tint = COLOR_HOVER_RED_NUM2;
                this.speakerButton.tint = COLOR_CLEAR_NUM;
            });
            this.muteButton.events.onInputOut.add(() => {
                this.muteButton.tint = COLOR_CLEAR_NUM;
            });
        }

        private createMenuExitButton(): void {
            this.exitButton = this.game.add.sprite(100, 450, "menuButton2");
            this.exitButton.scale.setTo(0.27, 0.2);
            this.exitButton.inputEnabled = true;

            this.exitButtonText = this.game.add.text(0, 0, "EXIT", { font: FONT_ACME, fill: BOTTOM_BUTTON_COLOR });
            this.exitButtonText.anchor.set(-2.4, -0.3);
            this.exitButtonText.position = this.exitButton.position;

            this.exitButton.events.onInputOver.add(() => {
                this.exitButtonText.fill = BOTTOM_BUTTON_COLOR_HOVER;
            });
            this.exitButton.events.onInputOut.add(() => {
                this.exitButtonText.fill = BOTTOM_BUTTON_COLOR;
            });
            this.exitButton.events.onInputDown.add(() => {
                if (this.playMusic) {
                    this.clickIconSound();
                }
                this.game.destroy();
                // @ts-ignore: for cordova stuff
                if (navigator.app) {
                    // @ts-ignore: for cordova stuff
                    navigator.app.exitApp();
                    // @ts-ignore: for cordova stuff
                } else if (navigator.device) {
                    // @ts-ignore: for cordova stuff
                    navigator.device.exitApp();
                } else {
                    window.close();
                }
            });
        }
    }
}
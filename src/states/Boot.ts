module TurnBasedBattles {

    export class Boot extends Phaser.State {

        public preload(): void {
            //this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            this.game.scale.scaleMode = Phaser.ScaleManager.EXACT_FIT;

            this.game.load.image("preloadBackground", "src/assets/backgrounds/preload-bg.jpg");
            this.game.load.image("mechanism-1", "src/assets/UI/mechanism-1.png");
            this.game.load.image("mechanism-2", "src/assets/UI/mechanism-2.png");
        }

        public create(): void {
            this.game.add.sprite(0, 0, "preloadBackground");
            this.game.state.start(PRELOAD_STATE, false, false);
        }
    }
}
module TurnBasedBattles {

    export class PlayerMeleeUnit extends Unit {

        currentState: UnitStates;
        cursor: Phaser.CursorKeys;

        constructor(game: Phaser.Game, x: number, y: number) {
            super(game, x, y);
            this.game = game;
            this.currentState = UnitStates.IDLE;
            this.x = x;
            this.y = y;
            this.pointerA_x = this.x;
            this.pointerA_y = this.y;
            this.step_x = 4; //FIXME

            this.cursor = this.game.input.keyboard.createCursorKeys();
            if (this.game.state.current.toString() === FIELD_BATTLE_STATE) {
                this.game.input.onDown.add(() => {
                    if ((this.game.input.y > (MAP_HEIGHT - this.battleFieldHeight)) &&
                        (this.canAttack)) {
                        this.pointerA_x = this.x;
                        this.pointerA_y = this.y;
                        this.pointerB_x = this.game.input.x + this.lvl * MOVEMENT;
                        this.pointerB_y = this.game.input.y;
                        this.step_y = Math.abs((this.pointerB_y - this.pointerA_y) / ((this.pointerB_x - this.pointerA_x) / this.step_x));
                        this.currentState = UnitStates.WALKRIGHT;
                        this.game.input.enabled = false;
                        this.attackFinished = false;
                        this.attackStarted = true;
                    }
                }, this);
            }
        }

        protected highlightUnit(): void {
            super.highlightUnit();
        }

        protected AttackSequence(): void {
            if (this.currentState === UnitStates.IDLE) {
                this.animation.animations.play("idle", null, true);
            }
            if (this.currentState === UnitStates.WALKRIGHT) {
                this.animation.animations.play("walk", null, true);
                if(this.continue) {
                    if (this.x <= this.initialPositionX + this.lvl * MOVEMENT - BUFFER) {
                        this.x += this.step_x;
                    } else {
                        this.currentState = UnitStates.IDLE;
                        this.continue = false;
                    }
                } else {
                    if (this.y < this.pointerB_y) {
                        this.y += this.step_y;
                    } else if (this.y > this.pointerB_y) {
                        this.y -= this.step_y;
                    }
                    if (this.x < this.pointerB_x - (4 * BUFFER + 30)) { // FIXME add const
                        this.x += this.step_x;
                    } else {
                        this.currentState = UnitStates.ATTACK;
                    }
                }
            }
            if (this.currentState === UnitStates.ATTACK) {
                this.animation.animations.play("attack", null, false)
                    .onComplete.add(() => {
                    this.currentState = UnitStates.WALKLEFT;
                });
            }
            if (this.currentState === UnitStates.WALKLEFT) {
                this.animation.animations.play("walk", null, true);
                this.animation.anchor.setTo(1, 0.5);
                this.animation.scale.set(-this.unitScale, this.unitScale);
                if (this.y > this.pointerA_y) {
                    this.y -= this.step_y;
                } else if (this.y < this.pointerA_y) {
                    this.y += this.step_y;
                }
                if (this.x > this.pointerA_x) {
                    this.x -= this.step_x;
                } else {
                    this.currentState = UnitStates.IDLE;
                    this.game.input.enabled = true;
                    this.attackFinished = true;
                    this.animation.anchor.setTo(this.defaultXanchor, 0.5);
                    this.animation.scale.set(this.unitScale, this.unitScale);
                }
            }
            this.animation.position.x = this.x;
            this.animation.position.y = this.y;
        }

        public update(): void {
            super.update();
            if (this.game.state.current.toString() === FIELD_BATTLE_STATE) {
                if (this.currentState === UnitStates.IDLE) {
                    this.animation.animations.play("idle");
                    if (this.isAttacked) {
                        this.currentState = UnitStates.HURT;
                    }
                } else if (this.currentState === UnitStates.HURT) {
                    if (this.health > 0) {
                        this.animation.animations.play("hurt", null, false)
                            .onComplete.add(() => {
                            this.isAttacked = false;
                            this.currentState = UnitStates.IDLE;
                        });
                    }
                    else {
                        this.animation.animations.play("die", null, false)
                            .onComplete.add(() => {
                            this.currentState = UnitStates.DEAD;
                        });
                    }
                } else if (this.currentState === UnitStates.DEAD) {
                    this.isHighlighted = false;
                    this.isAttackable = false;
                    this.isAttacked = false;
                    this.isAlive = false;
                } else {
                    this.AttackSequence();
                }
            }
        }
    }
}
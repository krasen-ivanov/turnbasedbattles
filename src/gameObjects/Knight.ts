﻿/// <reference path = "../../src/gameObjects/PlayerMeleeUnit.ts"/>

module TurnBasedBattles {

    export class Knight extends PlayerMeleeUnit {

        cursor: Phaser.CursorKeys;

        constructor(game: Phaser.Game, x: number, y: number, index: number = null) {
            super(game, x, y);
            this.game = game;
            this.currentState = UnitStates.IDLE;
            this.x = x;
            this.y = y;

            this.name = "Gold Knight";
            this.level = 1;
            this.strength = 60;
            this.agility = 5;
            this.health = 100;
            this.defaultHealth = this.health;
            this.defaultXanchor = 0;
            this.priceCost = 65;
            this.index = index;
            this.isMelee = true;
            this.unitScale = SIZE_KNIGHT;

            this.battleFieldHeight = MAP_HEIGHT / 2;
            //Add sprite
            this.rectImage = this.game.make.sprite(x, y, "knight-rect");
            this.circleImage = this.game.make.sprite(x, y, "knight-circle");

            this.animation = this.game.add.sprite(this.x, this.y, "knightGold");
            this.animation.animations.add("idle", [21, 22, 23, 24, 25, 26, 27], 30);
            this.animation.animations.add("walk", [28, 29, 30, 31, 32, 33, 34], 40);
            this.animation.animations.add("hurt", [14, 15, 16, 17, 18, 19, 20], 15);
            this.animation.animations.add("attack", [0, 1, 2, 3, 4, 5, 6], 30);
            this.animation.animations.add("die", [7, 8, 9, 10, 11, 12, 13], 15);
            this.animation.play("idle", 10, true);
            this.animation.anchor.setTo(this.defaultXanchor, 0.5);
            this.animation.scale.set(this.unitScale, this.unitScale); //FIXME
            this.animation.smoothed = false;

            this.highlightUnit();
        }

        public highlightUnit(): void {
            super.highlightUnit();
        }

        protected AttackSequence(): void {
            super.AttackSequence();
        }

        public update(): void {
            super.update();
        }
    }
}
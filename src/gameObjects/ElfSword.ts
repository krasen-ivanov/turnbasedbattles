/// <reference path = "../../src/gameObjects/EnemyMeleeUnit.ts"/>

module TurnBasedBattles {

    export class ElfSword extends EnemyMeleeUnit {

        cursor: Phaser.CursorKeys;

        constructor(game: Phaser.Game, x: number, y: number, index: number = null) {
            super(game, x, y);
            this.game = game;
            this.currentState = UnitStates.IDLE;
            this.x = x;
            this.y = y;

            this.name = "Elf Sword";
            this.level = 1;
            this.strength = 60;
            this.agility = 5;
            this.health = 90;
            this.defaultHealth = this.health;
            this.defaultXanchor = 1;
            this.priceCost = 65;
            this.index = index;
            this.isMelee = true;
            this.unitScale = SIZE_MAGE;

            this.battleFieldHeight = MAP_HEIGHT / 2;
            //Add sprite
            this.circleImage = this.game.make.sprite(x, y, "elfSword-circle");

            this.animation = this.game.add.sprite(this.x, this.y, "elfSword");
            this.animation.animations.add("idle", [0, 1, 2, 3, 4], 30);
            this.animation.animations.add("walk", [5, 6, 7, 8, 9], 30);
            this.animation.animations.add("hurt", [25, 26, 27, 28, 29], 15);
            this.animation.animations.add("attack", [20, 21, 22, 23, 24], 30);
            this.animation.animations.add("die", [30, 31, 32, 33, 34], 15);
            this.animation.play("idle", 10, true);
            this.animation.anchor.setTo(this.defaultXanchor, 0.5);
            this.animation.scale.set(-this.unitScale, this.unitScale); //FIXME
            this.animation.smoothed = false;

            this.highlightUnit();
        }

        public highlightUnit(): void {
            super.highlightUnit();
        }

        protected AttackSequence(): void {
            super.AttackSequence();
        }

        public update(): void {
            super.update();
        }
    }
}
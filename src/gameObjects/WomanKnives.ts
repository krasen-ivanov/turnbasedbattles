module TurnBasedBattles {

    export class WomanKnives extends EnemyMeleeUnit {

        cursor: Phaser.CursorKeys;

        constructor(game: Phaser.Game, x: number, y: number, index: number = null) {
            super(game, x, y);
            this.game = game;
            this.currentState = UnitStates.IDLE;
            this.x = x;
            this.y = y;

            this.name = "Woman Knives";
            this.level = 3;
            this.strength = 90;
            this.agility = 9;
            this.health = 70;
            this.defaultHealth = this.health;
            this.defaultXanchor = 1;
            this.priceCost = 90;
            this.index = index;
            this.isMelee = true;
            this.unitScale = SIZE_ARCHER;

            this.battleFieldHeight = MAP_HEIGHT / 2;
            //Add sprite
            this.circleImage = this.game.make.sprite(x, y, "womanKnives-circle");

            this.animation = this.game.add.sprite(this.x, this.y, "womanKnives");
            this.animation.animations.add("idle", [0, 1, 2, 3, 4], 30);
            this.animation.animations.add("walk", [5, 6, 7, 8, 9], 30);
            this.animation.animations.add("die", [30, 31, 32, 33, 34], 15);
            this.animation.animations.add("attack", [20, 21, 22, 23, 24], 20);
            this.animation.animations.add("hurt", [25, 26, 27, 28, 29], 20);
            this.animation.play("idle", 10, true);
            this.animation.anchor.setTo(this.defaultXanchor, 0.5);
            this.animation.scale.set(-this.unitScale, this.unitScale);
            this.animation.smoothed = false;

            this.highlightUnit();
        }

        protected AttackSequence(): void {
            super.AttackSequence();
        }

        public update(): void {
            super.update();
        }
    }
}
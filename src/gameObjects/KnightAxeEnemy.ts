module TurnBasedBattles {

    export class KnightAxeEnemy extends EnemyMeleeUnit {

        cursor: Phaser.CursorKeys;

        constructor(game: Phaser.Game, x: number, y: number, index: number = null) {
            super(game, x, y);
            this.game = game;
            this.currentState = UnitStates.IDLE;
            this.x = x;
            this.y = y;

            this.name = "Knight Axe Enemy";
            this.level = 1;
            this.strength = 120;
            this.agility = 5;
            this.health = 180;
            this.defaultHealth = this.health;
            this.defaultXanchor = 1;
            this.index = index;
            this.isMelee = true;
            this.unitScale = SIZE_KNIGHT_AXE;

            this.priceCost = 200;

            this.battleFieldHeight = MAP_HEIGHT / 2;
            //Add sprite
            this.circleImage = this.game.make.sprite(x, y, "knightAxeEnemy-circle");

            this.animation = this.game.add.sprite(this.x, this.y, "knightAxeEnemy");
            this.animation.animations.add("idle", [14, 15, 16, 17, 18, 19, 20], 30);
            this.animation.animations.add("walk", [21, 22, 23, 24, 25, 26, 27], 40);
            this.animation.animations.add("hurt", [35, 36, 37, 38, 39, 40, 41], 15);
            this.animation.animations.add("attack", [0, 1, 2, 3, 4, 5, 6], 30);
            this.animation.animations.add("die", [7, 8, 9, 10, 11, 12, 13], 15);
            this.animation.play("idle", 10, true);
            this.animation.anchor.setTo(this.defaultXanchor, 0.5);
            this.animation.scale.set(-this.unitScale, this.unitScale);
            this.animation.smoothed = false;

            this.highlightUnit();
        }

        protected AttackSequence(): void {
            super.AttackSequence();
        }

        public update(): void {
            super.update();
        }
    }
}
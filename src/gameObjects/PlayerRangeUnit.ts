/// <reference path = "../../src/gameObjects/Unit.ts"/>

module TurnBasedBattles {

    export class PlayerRangeUnit extends Unit {

        currentState: UnitStates;
        cursor: Phaser.CursorKeys;

        constructor(game: Phaser.Game, x: number, y: number) {
            super(game, x, y);
            this.game = game;
            this.currentState = UnitStates.IDLE;
            this.x = x;
            this.y = y;
            this.pointerA_x = this.x;
            this.pointerA_y = this.y;
            this.step_x = 4; // FIXME

            this.cursor = this.game.input.keyboard.createCursorKeys();
            if (this.game.state.current.toString() === FIELD_BATTLE_STATE) {
                this.game.input.onDown.add(() => {
                    if ((this.game.input.y > (MAP_HEIGHT - this.battleFieldHeight)) &&
                        (this.canAttack)) {
                        this.pointerA_x = this.x;
                        this.pointerA_y = this.y;
                        this.pointerB_x = this.game.input.x + this.lvl * MOVEMENT;
                        this.pointerB_y = this.game.input.y;
                        this.step_y = Math.abs((this.pointerB_y - this.pointerA_y) / ((this.pointerB_x - this.pointerA_x) / this.step_x));
                        this.currentState = UnitStates.ATTACK;
                        this.game.input.enabled = false;
                        this.attackFinished = false;
                        this.attackStarted = true;
                    }
                }, this);
            }
        }

        protected highlightUnit(): void {
            super.highlightUnit();
        }

        protected AttackSequence(): void {
            if (this.currentState === UnitStates.IDLE) {
                this.animation.animations.play("idle", null, true);
            }
            if (this.currentState === UnitStates.WALKRIGHT) {
                this.animation.animations.play("walk", null, true);
                if (this.continue) {
                    if (this.x <= this.initialPositionX + this.lvl * MOVEMENT - BUFFER) {
                        this.x += this.step_x;
                    } else {
                        this.currentState = UnitStates.IDLE;
                    }
                }
            }
            if (this.currentState === UnitStates.ATTACK) {
                this.animation.animations.play("attack", null, false)
                    .onComplete.add(() => {
                        this.currentState = UnitStates.IDLE;
                        this.game.input.enabled = true;
                        this.attackFinished = true;
                    });
            }
            this.animation.position.x = this.x;
            this.animation.position.y = this.y;
        }

        public update(): void {
            super.update();
            if (this.game.state.current.toString() === FIELD_BATTLE_STATE) {
                if (this.currentState === UnitStates.IDLE) {
                    this.animation.animations.play("idle");
                    if (this.isAttacked) {
                        this.currentState = UnitStates.HURT;
                    }
                } else if (this.currentState === UnitStates.HURT) {
                    if (this.health > 0) {
                        this.animation.animations.play("hurt", null, false)
                            .onComplete.add(() => {
                                this.isAttacked = false;
                                this.currentState = UnitStates.IDLE;
                            });
                    } else {
                        this.animation.animations.play("die", null, false)
                            .onComplete.add(() => {
                                this.currentState = UnitStates.DEAD;
                            });
                    }
                } else if (this.currentState === UnitStates.DEAD) {
                    this.isHighlighted = false;
                    this.isAttackable = false;
                    this.isAttacked = false;
                    this.isAlive = false;
                } else {
                    this.AttackSequence();
                }
            }
        }
    }
}
﻿module TurnBasedBattles {

    import Group = Phaser.Group;

    export class FieldBattle extends Phaser.State {

        ui: DrawUi;
        unitFormation: Array<Unit>;
        cursor: Phaser.Sprite;
        backgroundBattleField: Phaser.Sprite;
        MainPUI_X: Phaser.Sprite;
        highlightSignal: Phaser.Signal;
        attackSignal: Phaser.Signal;
        playerDeadSignal: Phaser.Signal;
        enemiesDeadSignal: Phaser.Signal;
        moveCameraSignal: Phaser.Signal;
        cursors: Phaser.CursorKeys;
        cam: Cam;
        armyInfo: string;

        unit1enemy: Unit;
        unit2enemy: Unit;
        unit3enemy: Unit;
        unit4enemy: Unit;
        unit5enemy: Unit;
        unit6enemy: Unit;

        currentUnit: any;
        nextUnit: any;
        tempUnit: any;

        enemiesDead: boolean;
        playerDead: boolean;
        activeGame: boolean;
        levelComplete: boolean;

        lvl: number;
        mapName: string;

        public init(uiClass: DrawUi, selectedUnitFormation: Array<Unit>, mapName: string, isComplete: boolean): void {
            this.ui = uiClass;
            this.unitFormation = selectedUnitFormation;
            this.mapName = mapName;
            this.levelComplete = isComplete;

            //console.log("The given array of units from MAP: ");
            //console.log(this.unitFormation);
        }

        public create(): void {
            this.cam = new Cam(this.game, 640, 360);
            this.loadBattleFieldBackground(this.mapName);
            this.game.world.setBounds(0, 0, MAP_WIDTH, MAP_HEIGHT);
            this.game.physics.startSystem(Phaser.Physics.P2JS);
            this.lvl = 0;
            this.activeGame = true;
            this.playerDead = false;
            this.enemiesDead = false;
            this.highlightSignal = new Phaser.Signal();
            this.attackSignal = new Phaser.Signal();
            this.playerDeadSignal = new Phaser.Signal();
            this.enemiesDeadSignal = new Phaser.Signal();
            this.moveCameraSignal = new Phaser.Signal();

            for (let index: number = 0; index < this.unitFormation.length; index++) {
                const unit: Unit = this.unitFormation[index];

                if (unit === null) {
                    //console.log("Empty slot. Null");
                } else if (this.unitFormation[index] && (unit.name === GOLD_KNIGHT_STR)) {
                    this.unitFormation[index] = new Knight(this.game, PUNIT_ARRAY_X[index], PUNIT_ARRAY_Y[index], index + 1);
                } else if (this.unitFormation[index] && (unit.name === ELF_MAGE_STR)) {
                    this.unitFormation[index] = new Mage(this.game, PUNIT_ARRAY_X[index], PUNIT_ARRAY_Y[index], index + 1);
                } else if (this.unitFormation[index] && (unit.name === WOMAN_ARCHER_STR)) {
                    this.unitFormation[index] = new Archer(this.game, PUNIT_ARRAY_X[index], PUNIT_ARRAY_Y[index], index + 1);
                } else if (this.unitFormation[index] && (unit.name === "Knight Spear")) {
                    this.unitFormation[index] = new KnightSpear(this.game, PUNIT_ARRAY_X[index], PUNIT_ARRAY_Y[index], index + 1);
                } else if (this.unitFormation[index] && (unit.name === "Knight Axe")) {
                    this.unitFormation[index] = new KnightAxe(this.game, PUNIT_ARRAY_X[index], PUNIT_ARRAY_Y[index], index + 1);
                } else {
                    //console.log("The " + unit.name + " is missing!");
                }
            }

            this.EnemyDeclaration(this.mapName);

            this.currentUnit = new Unit(this.game, 0, 0);
            this.nextUnit = new Unit(this.game, 0, 0);

            this.game.physics.p2.enable(this.cam.animation);

            this.cam.animation.body.fixedRotation = true;

            //console.log("DRAW UI");
            this.ui.drawFieldBattleStateUI();
            this.InitialFillUiSlots();
            this.cursors = this.game.input.keyboard.createCursorKeys();

            this.game.camera.follow(this.cam.animation, Phaser.Camera.FOLLOW_LOCKON, 0.1, 0.1);
            this.game.add.existing(this.cam.animation);

            this.SpawnEnemies();

            this.MoveUiSlots();

            this.currentUnit = this.checkIfNull(this.unitFormation[0], this.unit1enemy, this.unitFormation[1], this.unit2enemy,
                this.unitFormation[2], this.unit3enemy, this.unitFormation[3], this.unit4enemy, this.unitFormation[4],
                this.unit5enemy, this.unitFormation[5], this.unit6enemy);
            this.nextUnit = this.pickNextUnit();
            this.currentUnit.canAttack = true;

            this.enemiesDead = false;

            this.attackSignal.add((canAttack, attackFinished, attackStarted) => {
                if (canAttack && attackFinished && attackStarted) {
                    this.attackOrder();
                    //console.log("attack order DISPATCHED");
                }
                if (this.unitExists(this.currentUnit)) {
                    //console.log("1. CUR: " + this.currentUnit.name + " NEXT: " + this.nextUnit.name);
                    //console.log("1. CUR: " + this.currentUnit.canAttack+this.currentUnit.attackFinished
                    //                       + this.currentUnit.attackStarted + "   "+
                    //                       + this.nextUnit.canAttack + this.nextUnit.attackFinished+
                    //                         this.nextUnit.attackStarted);
                    if (!this.currentUnit.isDone) {
                        this.CurrentUnitAttack();
                        //console.log("LETS ATTACK NOW");
                    }
                } else {
                    this.pickCurrentUnit();
                    this.pickNextUnit();
                    this.currentUnit.canAttack = true;
                    //console.log("2. CUR: " + this.currentUnit.name + " NEXT: " + this.nextUnit.name);
                    //console.log("2. CUR: " + this.currentUnit.canAttack+this.currentUnit.attackFinished
                    //    + this.currentUnit.attackStarted + "   "+
                    //    + this.nextUnit.canAttack + this.nextUnit.attackFinished+
                    //    this.nextUnit.attackStarted);
                }
            });

            this.highlightSignal.add(() => {
                this.currentUnit.highlightUnit2();
            });

            this.playerDeadSignal.addOnce(() => {
                //console.log("YOU'RE DEAD!!!");
                this.lvl = 0;
                this.activeGame = false;
                this.BattleLost();
            });

            this.enemiesDeadSignal.add(() => {
                //console.log("ENEMIES ARE ALL DEAD!!!");
                this.lvl++;
                //console.log("LEVEL " + this.lvl);
                if (this.lvl > 2) {
                    this.activeGame = false;
                    this.BattleWon();
                }
                this.NextLVL();
            });

            this.moveCameraSignal.add(() => {
                if (this.activeGame && this.enemiesDead) {
                    if (this.AttackFinished(this.unitFormation)) {
                        this.MoveCamera();
                    }
                }
            });

            //console.log(this.game.state.current.toString());

        }

        public update(): void {
            this.cam.animation.body.setZeroVelocity();
            this.unitUpdate(this.unitFormation[0]);
            this.unitUpdate(this.unit1enemy);
            this.unitUpdate(this.unitFormation[1]);
            this.unitUpdate(this.unit2enemy);
            this.unitUpdate(this.unitFormation[2]);
            this.unitUpdate(this.unit3enemy);
            this.unitUpdate(this.unitFormation[3]);
            this.unitUpdate(this.unit4enemy);
            this.unitUpdate(this.unitFormation[4]);
            this.unitUpdate(this.unit5enemy);
            this.unitUpdate(this.unitFormation[5]);
            this.unitUpdate(this.unit6enemy);
            this.attackSignal.dispatch(this.currentUnit.canAttack, this.currentUnit.attackFinished, this.currentUnit.attackStarted);
            this.highlightSignal.dispatch(this.currentUnit.canAttack, this.currentUnit.attackStarted);
            this.moveCameraSignal.dispatch();
            //console.log(this.currentUnit.name+"can attack: "+this.currentUnit.canAttack+" AF: "+this.currentUnit.attackFinished+
            //    "AS: "+this.currentUnit.attackStarted+" "+this.nextUnit.name);
        }

        private EnemyDeclaration(map: string): void {
            switch (map) {
                case "Forest": {
                    this.unit1enemy = new TrollClub(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 1);
                    this.unit2enemy = new TrollHammer(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 2);
                    this.unit3enemy = new TrollBone(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 3);
                    this.unit4enemy = new TrollBone(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 4);
                    this.unit5enemy = new TrollHammer(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 5);
                    this.unit6enemy = new TrollClub(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 6);
                    break;
                }
                case "Mountain": {
                    this.unit1enemy = new KnightAxeEnemy(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 1);
                    this.unit2enemy = new KnightGoldEnemy(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 2);
                    this.unit3enemy = new KnightSpearEnemy(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 3);
                    this.unit4enemy = new KnightSpearEnemy(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 4);
                    this.unit5enemy = new KnightGoldEnemy(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 5);
                    this.unit6enemy = new KnightAxeEnemy(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 6);
                    break;
                }
                case "Waterfall": {
                    this.unit1enemy = new ElfSword(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 1);
                    this.unit2enemy = new ElfArcher(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 2);
                    this.unit3enemy = new ElfMageEnemy(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 3);
                    this.unit4enemy = new ElfMageEnemy(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 4);
                    this.unit5enemy = new ElfArcher(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 5);
                    this.unit6enemy = new ElfSword(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 6);
                    break;
                }
                case "PineForest": {
                    this.unit1enemy = new OrcAxe(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 1);
                    this.unit2enemy = new OrcHammer(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 2);
                    this.unit3enemy = new OrcSword(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 3);
                    this.unit4enemy = new OrcSword(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 4);
                    this.unit5enemy = new OrcHammer(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 5);
                    this.unit6enemy = new OrcAxe(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 6);
                    break;
                }
                case "PineForest2": {
                    this.unit1enemy = new WomanKnives(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 1);
                    this.unit2enemy = new WomanArcherEnemy(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 2);
                    this.unit3enemy = new WomanSword(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 3);
                    this.unit4enemy = new WomanSword(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 4);
                    this.unit5enemy = new WomanArcherEnemy(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 5);
                    this.unit6enemy = new WomanKnives(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 6);
                    break;
                }
                case "BossGround": {
                    this.unit1enemy = new KnightSpearEnemy(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 1);
                    this.unit2enemy = new Boss(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 2);
                    this.unit3enemy = new KnightSpearEnemy(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 3);
                    this.unit4enemy = new KnightAxeEnemy(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 4);
                    this.unit5enemy = new ElfArcher(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 5);
                    this.unit6enemy = new KnightAxeEnemy(this.game, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, 6);
                    break;
                }
                default: {
                    break;
                }
            }
        }

        private InitialFillUiSlots(): void {
            for (let index: number = 0; index < this.unitFormation.length; index++) {
                const unit: Unit = this.unitFormation[index];

                if (unit === null) {
                    //console.log("Empty slot at InitialFillUiSlots");
                } else {
                    this.FillUiSlot(this.unitFormation[index], HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, true);
                }
            }

            this.FillUiSlot(this.unit1enemy, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, false);
            this.FillUiSlot(this.unit2enemy, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, false);
            this.FillUiSlot(this.unit3enemy, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, false);
            this.FillUiSlot(this.unit4enemy, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, false);
            this.FillUiSlot(this.unit5enemy, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, false);
            this.FillUiSlot(this.unit6enemy, HIDE_FROM_SCREEN, HIDE_FROM_SCREEN, false);
        }

        private MoveUiSlots(): void {

            for (let index: number = 0; index < this.unitFormation.length; index++) {
                const unit: Unit = this.unitFormation[index];

                if (unit === null) {
                    //console.log("Empty slot at MoveUiSlots");
                } else {
                    this.MoveUiSlot(this.unitFormation[index], PSLOT_ARRAY_X[index], PSLOT_ARRAY_Y[index], true);
                }
            }

            this.MoveUiSlot(this.unit1enemy, E1SLOT_POS_X, E1SLOT_POS_Y, false);
            this.MoveUiSlot(this.unit2enemy, E2SLOT_POS_X, E2SLOT_POS_Y, false);
            this.MoveUiSlot(this.unit3enemy, E3SLOT_POS_X, E3SLOT_POS_Y, false);
            this.MoveUiSlot(this.unit4enemy, E4SLOT_POS_X, E4SLOT_POS_Y, false);
            this.MoveUiSlot(this.unit5enemy, E5SLOT_POS_X, E5SLOT_POS_Y, false);
            this.MoveUiSlot(this.unit6enemy, E6SLOT_POS_X, E6SLOT_POS_Y, false);

        }

        private MoveUiSlot(Unit: Unit, x: number, y: number, player: boolean): void {
            this.ClearUiSlot(Unit);
            if (this.unitExists(Unit)) {
                if (player) {
                    // FIXME fix the magic numbers
                    Unit.circleImage.x = x + 22;
                    Unit.circleImage.y = y + 5;
                    Unit.HPbarBottom.x = x + 18;
                    Unit.HPbarBottom.y = y + 70;
                    Unit.HPbarTop.x = x + 18;
                    Unit.HPbarTop.y = y + 70;
                } else {
                    // FIXME fix the magic numbers
                    Unit.circleImage.x = x - 22;
                    Unit.circleImage.y = y + 5;
                    Unit.HPbarBottom.x = x - 99;
                    Unit.HPbarBottom.y = y + 70;
                    Unit.HPbarTop.x = x - 99;
                    Unit.HPbarTop.y = y + 70;
                }
                Unit.circleImage.fixedToCamera = true;
                Unit.HPbarBottom.fixedToCamera = true;
                Unit.HPbarTop.fixedToCamera = true;
            }
        }

        private ClearUiSlot(Unit: Unit): void {

            if (Unit === null) {
                //console.log("Empty slot at ClearUiSlot");
            } else {
                Unit.circleImage.fixedToCamera = false;
                Unit.HPbarBottom.fixedToCamera = false;
                Unit.HPbarTop.fixedToCamera = false;
                Unit.circleImage.x = HIDE_FROM_SCREEN;
                Unit.circleImage.y = HIDE_FROM_SCREEN;
                Unit.HPbarBottom.x = HIDE_FROM_SCREEN;
                Unit.HPbarBottom.y = HIDE_FROM_SCREEN;
                Unit.HPbarTop.x = HIDE_FROM_SCREEN;
                Unit.HPbarTop.y = HIDE_FROM_SCREEN;
            }
        }

        private FillUiSlot(Unit: Unit, x: number, y: number, player: boolean): void {
            if (player) {
                // FIXME fix the magic numbers
                Unit.circleImage = this.game.add.sprite(x + 22, y + 5, Unit.circleImage.key); // FIXME Make this adjustment prettier
                Unit.circleImage.scale.setTo(1, 1);
                Unit.HPbarBottom = this.game.add.sprite(x, y + 70, Unit.HPbarBottom.key);
                Unit.HPbarBottom.anchor.setTo(0, 0.5);
                Unit.HPbarBottom.scale.setTo(SIZE_HP, SIZE_HP - 0.02);
                Unit.HPbarTop = this.game.add.sprite(x, y + 70, Unit.HPbarTop.key);
                Unit.HPbarTop.anchor.setTo(0, 0.5);
                Unit.HPbarTop.scale.setTo(SIZE_HP, SIZE_HP - 0.02);

            } else {
                // FIXME fix the magic numbers
                Unit.circleImage = this.game.add.sprite(x - 22, y + 5, Unit.circleImage.key); // FIXME Make this adjustment prettier
                Unit.circleImage.scale.setTo(-1, 1);
                Unit.HPbarBottom = this.game.add.sprite(x, y + 70, Unit.HPbarBottom.key);
                Unit.HPbarBottom.anchor.setTo(0, 0.5);
                Unit.HPbarBottom.scale.setTo(SIZE_HP, SIZE_HP - 0.02);
                Unit.HPbarTop = this.game.add.sprite(x, y + 70, Unit.HPbarTop.key);
                Unit.HPbarTop.anchor.setTo(0, 0.5);
                Unit.HPbarTop.scale.setTo(SIZE_HP, SIZE_HP - 0.02);
            }
        }

        private BurnDeadBodies(Units: Array<Unit>): void {
            for (var i: number = 0; i < Units.length; i++) {
                if (!this.unitExists(Units[i])) {
                    Units.splice(i, 1, null);
                }
            }
        }

        private BattleLost(): void {
            let gameLost: Phaser.Image;

            this.levelComplete = false;
            this.BurnDeadBodies(this.unitFormation);

            gameLost = this.game.add.sprite(this.game.width * 0.5, this.game.height * 0.5, "gameLost");
            gameLost.alpha = 0;
            gameLost.anchor.set(0.5);
            gameLost.fixedToCamera = true;
            this.game.add.tween(gameLost).to({ alpha: 1 }, 1500, Phaser.Easing.Default, true);
            //gameLost.inputEnabled = true;
            this.ui.stopMusic();
            this.ui.gameLostMusic();
            DrawUi.avaliableUserMoney += 100;
            this.camera.fade(0x000000, 6500);
            this.camera.onFadeComplete.add(() => {
                this.game.state.start(TOWN_STATE, true, false, this.ui, this.unitFormation, this.levelComplete);
                //console.log("The units after the battle is lost");
                //console.log(this.unitFormation);
            });
            // gameLost.events.onInputDown.add(() => {
            //     this.game.state.start(TOWN_STATE, true, false, this.ui, this.unitFormation);
            // });
        }

        private BattleWon(): void {
            let gameWon: Phaser.Image;
            // TODO: add music
            this.levelComplete = true;
            this.BurnDeadBodies(this.unitFormation);

            gameWon = this.game.add.sprite(this.game.width * 0.5, this.game.height * 0.5, "gameWon");
            gameWon.alpha = 0;
            gameWon.anchor.set(0.5);
            gameWon.fixedToCamera = true;
            gameWon.inputEnabled = true;
            this.game.add.tween(gameWon).to({ alpha: 1 }, 1500, Phaser.Easing.Default, true);
            this.ui.stopMusic();
            this.ui.gameWinMusic();
            DrawUi.avaliableUserMoney += 250;
            gameWon.events.onInputDown.add(() => {
                this.game.state.start(MAP_STATE, true, false, this.ui, this.unitFormation, this.mapName, this.levelComplete);
                //TODO: have to splice the array with the dead units
                //console.log("The units after the battle is over");
                //console.log(this.unitFormation);
            });
        }

        private PickPosition(Unit: Unit, number1: number, number2: number, number3: number,
            number4: number, number5: number, number6: number, temp: number = null): number {
            switch (Unit.index) {
                case 1: {
                    temp = number1;
                    break;
                }
                case 2: {
                    temp = number2;
                    break;
                }
                case 3: {
                    temp = number3;
                    break;
                }
                case 4: {
                    temp = number4;
                    break;
                }
                case 5: {
                    temp = number5;
                    break;
                }
                case 6: {
                    temp = number6;
                    break;
                }
                default: {
                    //console.log("ERROR!");
                    break;
                }
            }
            return temp;
        }

        private SpawnEnemies(): void {
            switch (this.lvl) {
                case 0: {
                    if (this.mapName === "BossGround") {
                        this.RespawnEnemy(this.unit1enemy);
                        this.RespawnEnemy(this.unit3enemy);
                        this.RespawnEnemy(this.unit5enemy);
                    } else {
                        this.RespawnEnemy(this.unit1enemy);
                        this.RespawnEnemy(this.unit3enemy);
                    }
                    break;
                }
                case 1: {
                    if (this.mapName === "BossGround") {
                        this.RespawnEnemy(this.unit4enemy);
                        this.RespawnEnemy(this.unit6enemy);
                    } else {
                        this.RespawnEnemy(this.unit2enemy);
                        this.RespawnEnemy(this.unit4enemy);
                        this.RespawnEnemy(this.unit6enemy);
                    }
                    break;
                }
                case 2: {
                    if (this.mapName === "BossGround") {
                        this.RespawnEnemy(this.unit2enemy);
                    } else {
                        this.RespawnEnemy(this.unit1enemy);
                        this.RespawnEnemy(this.unit3enemy);
                        this.RespawnEnemy(this.unit5enemy);
                    }
                    break;
                }
                default: {

                    break;
                }
            }
            this.MoveUiSlots();
        }

        private NextLVL(): void {
            this.NextLVLperUnit(this.unitFormation[0]);
            this.NextLVLperUnit(this.unitFormation[1]);
            this.NextLVLperUnit(this.unitFormation[2]);
            this.NextLVLperUnit(this.unitFormation[3]);
            this.NextLVLperUnit(this.unitFormation[4]);
            this.NextLVLperUnit(this.unitFormation[5]);
        }

        private NextLVLperUnit(Unit: Unit): void {
            if (this.unitExists(Unit)) {
                Unit.lvl = this.lvl;
            }
        }

        private RespawnEnemy(Unit: Unit): void {
            if (!Unit.isAlive) {
                Unit.x = this.PickPosition(Unit, EUNIT1_POS_X, EUNIT2_POS_X, EUNIT3_POS_X,
                    EUNIT4_POS_X, EUNIT5_POS_X, EUNIT6_POS_X) + this.lvl * MOVEMENT;
                Unit.y = this.PickPosition(Unit, EUNIT1_POS_Y, EUNIT2_POS_Y, EUNIT3_POS_Y,
                    EUNIT4_POS_Y, EUNIT5_POS_Y, EUNIT6_POS_Y);
                //console.log("CALCULATED POSITION Y IS " + Unit.y);
                Unit.animation.x = Unit.x;
                Unit.animation.y = Unit.y;
                Unit.pointerA_x = Unit.x;
                Unit.pointerA_y = Unit.y;
                Unit.isAlive = true;
                Unit.health = Unit.defaultHealth;
                Unit.HPbarTop.scale.setTo(0.27, 0.25);
                Unit.currentState = UnitStates.IDLE;
                Unit.lvl = this.lvl;
                Unit.animation.alpha = 1;
            }
        }

        private AttackFinished(units: Array<Unit>, temp: boolean = null): boolean {
            //return (unit1.attackFinished && unit2.attackFinished && unit3.attackFinished &&
            //    unit4.attackFinished && unit5.attackFinished && unit6.attackFinished);
            temp = true;
            for (var i: number = 0; i < units.length; i++) {
                if (this.unitExists(units[i])) {
                    temp = temp && units[i].attackFinished;
                }
            }
            return temp;
        }

        private MovePlayer(): void {
            this.MoveUnit(this.unitFormation[0]);
            this.MoveUnit(this.unitFormation[1]);
            this.MoveUnit(this.unitFormation[2]);
            this.MoveUnit(this.unitFormation[3]);
            this.MoveUnit(this.unitFormation[4]);
            this.MoveUnit(this.unitFormation[5]);
        }

        private MoveUnit(Unit: Unit): void {
            if (this.unitExists(Unit)) {
                Unit.continue = true;
                Unit.currentState = UnitStates.WALKRIGHT;
            }
        }

        private MoveCamera(): void {
            if (this.game.camera.x < this.lvl * MOVEMENT + BUFFER) {
                this.game.camera.x += 10;
                this.cam.animation.body.moveRight(240);
                //console.log("moving right");
                this.game.input.enabled = false;
                this.MovePlayer();
                this.SpawnEnemies();
            } else {
                this.enemiesDead = false;
                this.game.input.enabled = true;
            }
        }

        private EnemiesDead(temp: boolean = null): boolean {
            //console.log("Enemies DEAD: " + !(this.unitExists(this.unit1enemy) || this.unitExists(this.unit2enemy) ||
            //    this.unitExists(this.unit3enemy) || this.unitExists(this.unit4enemy) ||
            //    this.unitExists(this.unit5enemy) || this.unitExists(this.unit6enemy)));
            return temp = !(this.unitExists(this.unit1enemy) || this.unitExists(this.unit2enemy) ||
                this.unitExists(this.unit3enemy) || this.unitExists(this.unit4enemy) ||
                this.unitExists(this.unit5enemy) || this.unitExists(this.unit6enemy));
        }

        private PlayerDead(temp: boolean = null): boolean {
            return temp = !(this.unitExists(this.unitFormation[0]) || this.unitExists(this.unitFormation[1]) ||
                this.unitExists(this.unitFormation[2]) || this.unitExists(this.unitFormation[3]) ||
                this.unitExists(this.unitFormation[4]) || this.unitExists(this.unitFormation[5]));
        }

        private unitExists(Unit: Unit, temp: boolean = null): boolean {
            if (Unit != null) {
                if (Unit.isAlive) {
                    temp = true;
                } else {
                    temp = false;
                }
            } else {
                temp = false;
            }
            return temp;
        }

        private unitUpdate(Unit: Unit): void {
            if (this.unitExists(Unit)/*Unit != null*/) {
                Unit.update();
            }
        }

        private checkIfNull(unit1: Unit, unit2: Unit, unit3: Unit, unit4: Unit, unit5: Unit, unit6: Unit,
            unit7: Unit, unit8: Unit, unit9: Unit, unit10: Unit, unit11: Unit, unit12: Unit): Unit {
            if (this.unitExists(unit1)) {
                return unit1;
            } else if (this.unitExists(unit2)) {
                return unit2;
            } else if (this.unitExists(unit3)) {
                return unit3;
            } else if (this.unitExists(unit4)) {
                return unit4;
            } else if (this.unitExists(unit5)) {
                return unit5;
            } else if (this.unitExists(unit6)) {
                return unit6;
            } else if (this.unitExists(unit7)) {
                return unit7;
            } else if (this.unitExists(unit8)) {
                return unit8;
            } else if (this.unitExists(unit9)) {
                return unit9;
            } else if (this.unitExists(unit10)) {
                return unit10;
            } else if (this.unitExists(unit11)) {
                return unit11;
            } else if (this.unitExists(unit12)) {
                return unit12;
            } else {
                return this.currentUnit;
            }
        }

        private pickCurrentUnit(): void {
            if (this.unitExists(this.nextUnit)) {
                this.currentUnit = this.nextUnit;
            }
        }

        private pickNextUnit(): Unit {
            if ((this.unitExists(this.unitFormation[0])) && (this.currentUnit === this.unitFormation[0])) {
                this.nextUnit = this.checkIfNull(this.unit1enemy, this.unitFormation[1], this.unit2enemy,
                    this.unitFormation[2], this.unit3enemy, this.unitFormation[3], this.unit4enemy, this.unitFormation[4], this.unit5enemy,
                    this.unitFormation[5], this.unit6enemy, this.unitFormation[0]);
                //console.log("I'm here 2");
            } else if ((this.unitExists(this.unit1enemy)) && (this.currentUnit === this.unit1enemy)) {
                this.nextUnit = this.checkIfNull(this.unitFormation[1], this.unit2enemy, this.unitFormation[2],
                    this.unit3enemy, this.unitFormation[3], this.unit4enemy, this.unitFormation[4], this.unit5enemy, this.unitFormation[5],
                    this.unit6enemy, this.unitFormation[0], this.unit1enemy);
                //console.log("I'm here 3");
            } else if ((this.unitExists(this.unitFormation[1])) && (this.currentUnit === this.unitFormation[1])) {
                this.nextUnit = this.checkIfNull(this.unit2enemy, this.unitFormation[2], this.unit3enemy, this.unitFormation[3],
                    this.unit4enemy, this.unitFormation[4], this.unit5enemy, this.unitFormation[5], this.unit6enemy, this.unitFormation[0],
                    this.unit1enemy, this.unitFormation[1]);
                //console.log("I'm here 4");
            } else if ((this.unitExists(this.unit2enemy)) && (this.currentUnit === this.unit2enemy)) {
                this.nextUnit = this.checkIfNull(this.unitFormation[2], this.unit3enemy, this.unitFormation[3], this.unit4enemy,
                    this.unitFormation[4], this.unit5enemy, this.unitFormation[5], this.unit6enemy, this.unitFormation[0], this.unit1enemy,
                    this.unitFormation[1], this.unit2enemy);
                //console.log("I'm here 5");
            } else if ((this.unitExists(this.unitFormation[2])) && (this.currentUnit === this.unitFormation[2])) {
                this.nextUnit = this.checkIfNull(this.unit3enemy, this.unitFormation[3], this.unit4enemy, this.unitFormation[4],
                    this.unit5enemy, this.unitFormation[5], this.unit6enemy, this.unitFormation[0], this.unit1enemy, this.unitFormation[1],
                    this.unit2enemy, this.unitFormation[2]);
                //console.log("I'm here 6");
            } else if ((this.unitExists(this.unit3enemy)) && (this.currentUnit === this.unit3enemy)) {
                this.nextUnit = this.checkIfNull(this.unitFormation[3], this.unit4enemy,
                    this.unitFormation[4], this.unit5enemy, this.unitFormation[5], this.unit6enemy, this.unitFormation[0], this.unit1enemy,
                    this.unitFormation[1], this.unit2enemy, this.unitFormation[2], this.unit3enemy);
                //console.log("I'm here 7");
            } else if ((this.unitExists(this.unitFormation[3])) && (this.currentUnit === this.unitFormation[3])) {
                this.nextUnit = this.checkIfNull(this.unit4enemy, this.unitFormation[4], this.unit5enemy, this.unitFormation[5],
                    this.unit6enemy, this.unitFormation[0], this.unit1enemy, this.unitFormation[1], this.unit2enemy, this.unitFormation[2],
                    this.unit3enemy, this.unitFormation[3]);
                //console.log("I'm here 8");
            } else if ((this.unitExists(this.unit4enemy)) && (this.currentUnit === this.unit4enemy)) {
                this.nextUnit = this.checkIfNull(this.unitFormation[4], this.unit5enemy, this.unitFormation[5], this.unit6enemy,
                    this.unitFormation[0], this.unit1enemy, this.unitFormation[1], this.unit2enemy, this.unitFormation[2], this.unit3enemy,
                    this.unitFormation[3], this.unit4enemy);
                //console.log("I'm here 9");
            } else if ((this.unitExists(this.unitFormation[4])) && (this.currentUnit === this.unitFormation[4])) {
                this.nextUnit = this.checkIfNull(this.unit5enemy, this.unitFormation[5], this.unit6enemy,
                    this.unitFormation[0], this.unit1enemy, this.unitFormation[1],
                    this.unit2enemy, this.unitFormation[2], this.unit3enemy, this.unitFormation[3], this.unit4enemy, this.unitFormation[4]);
                //console.log("I'm here 10");
            } else if ((this.unitExists(this.unit5enemy)) && (this.currentUnit === this.unit5enemy)) {
                this.nextUnit = this.checkIfNull(this.unitFormation[5], this.unit6enemy,
                    this.unitFormation[0], this.unit1enemy, this.unitFormation[1], this.unit2enemy,
                    this.unitFormation[2], this.unit3enemy, this.unitFormation[3], this.unit4enemy, this.unitFormation[4], this.unit5enemy);
                //console.log("I'm here 11");
            } else if ((this.unitExists(this.unitFormation[5])) && (this.currentUnit === this.unitFormation[5])) {
                this.nextUnit = this.checkIfNull(this.unit6enemy,
                    this.unitFormation[0], this.unit1enemy, this.unitFormation[1], this.unit2enemy, this.unitFormation[2],
                    this.unit3enemy, this.unitFormation[3], this.unit4enemy, this.unitFormation[4], this.unit5enemy, this.unitFormation[5]);
                //console.log("I'm here 12");
            } else if ((this.unitExists(this.unit6enemy)) && (this.currentUnit === this.unit6enemy)) {
                this.nextUnit = this.checkIfNull(
                    this.unitFormation[0], this.unit1enemy, this.unitFormation[1], this.unit2enemy, this.unitFormation[2], this.unit3enemy,
                    this.unitFormation[3], this.unit4enemy, this.unitFormation[4], this.unit5enemy, this.unitFormation[5], this.unit6enemy);
                //console.log("I'm here 1");
            } else {
                this.nextUnit = this.checkIfNull(
                    this.unitFormation[0], this.unit1enemy, this.unitFormation[1], this.unit2enemy, this.unitFormation[2], this.unit3enemy,
                    this.unitFormation[3], this.unit4enemy, this.unitFormation[4], this.unit5enemy, this.unitFormation[5], this.unit6enemy);
            }
            //console.log("ALIVE CUR?"+this.currentUnit.isAlive);
            //console.log("ALIVE NXT?"+this.nextUnit.isAlive);
            return this.nextUnit;
        }

        private attackOrder(): void {
            if (this.nextUnit.isAlive) {
                if (this.currentUnit.canAttack && this.currentUnit.attackFinished && this.currentUnit.attackStarted) {
                    this.currentUnit.canAttack = false;
                    this.currentUnit.attackStarted = false;
                    this.currentUnit.isDone = false;
                    this.pickCurrentUnit();
                    this.pickNextUnit();
                    this.currentUnit.canAttack = true;
                    if (this.currentUnit.isEnemy && this.currentUnit.health > 0) {
                        this.ActivateEnemy();
                    }
                }
            } else {
                this.pickCurrentUnit(); //FIXME: Review if needed.
                this.pickNextUnit();
                this.currentUnit.canAttack = true;
            }
            //console.log(this.currentUnit, this.nextUnit);
        }

        private CurrentUnitAttack(): void {
            if (this.currentUnit.currentState === UnitStates.ATTACK) {
                if (!this.currentUnit.isEnemy) {
                    this.currentUnit.DoDamage(this.unit1enemy, this.unit2enemy, this.unit3enemy,
                        this.unit4enemy, this.unit5enemy, this.unit6enemy, this.currentUnit.strength);
                    this.ui.attackSound(); /////
                } else {
                    this.currentUnit.DoDamage(this.unitFormation[0], this.unitFormation[1], this.unitFormation[2],
                        this.unitFormation[3], this.unitFormation[4], this.unitFormation[5], this.currentUnit.strength);
                    this.ui.attackSound(); ////
                }
            }
            if (this.PlayerDead()) {
                this.playerDead = true;
                this.playerDeadSignal.dispatch();
            }
            if (this.EnemiesDead()) {
                this.enemiesDead = true;
                if (this.activeGame && this.AttackFinished(this.unitFormation)) {
                    this.enemiesDeadSignal.dispatch();
                }
            }
        }

        private ActivateEnemy(): void {
            if (this.activeGame) {
                this.tempUnit = this.checkIfNull(this.unitFormation[0], this.unitFormation[1], this.unitFormation[2],
                    this.unitFormation[3], this.unitFormation[4], this.unitFormation[5],
                    this.unitFormation[0], this.unitFormation[1], this.unitFormation[2], this.unitFormation[3],
                    this.unitFormation[4], this.unitFormation[5]);
                //console.log(this.activeGame);
                this.currentUnit.pointerB_x = this.tempUnit.x;
                this.currentUnit.pointerB_y = this.tempUnit.y;
                this.tempUnit.isAttackable = true;
                //console.log("Is Attackable " + this.tempUnit.isAttackable + " " + this.tempUnit.name);
                if (this.currentUnit.isMelee) {
                    this.currentUnit.currentState = UnitStates.WALKLEFT;
                } else {
                    this.currentUnit.currentState = UnitStates.ATTACK;
                }
                this.currentUnit.attackFinished = false;
                this.currentUnit.attackStarted = true;
            }
        }

        public render(): void {
            //this.game.debug.cameraInfo(this.game.camera, 32, 32);
        }

        private loadBattleFieldBackground(map: string): void {
            switch (map) {
                case "Forest": {
                    this.backgroundBattleField = this.game.add.sprite(0, 0, "battleFieldForest");
                    break;
                }
                case "Mountain": {
                    this.backgroundBattleField = this.game.add.sprite(0, 0, "battleFieldMountain");
                    break;
                }
                case "Waterfall": {
                    this.backgroundBattleField = this.game.add.sprite(0, 0, "battleFieldWaterfall");
                    break;
                }
                case "PineForest": {
                    this.backgroundBattleField = this.game.add.sprite(0, 0, "battleFieldPineForest");
                    break;
                }
                case "PineForest2": {
                    this.backgroundBattleField = this.game.add.sprite(0, 0, "battleFieldPineForest2");
                    break;
                }
                case "BossGround": {
                    this.backgroundBattleField = this.game.add.sprite(0, 0, "battleFieldBossGround");
                    break;
                }
                default: {
                    this.backgroundBattleField = this.game.add.sprite(0, 0, "battleFieldForest");
                    break;
                }
            }
        }
    }
}
module TurnBasedBattles {

    export class BattleUI {

        public game: Phaser.Game;

        private MainPUI: Phaser.Image;
        private MainEUI: Phaser.Image;

        private P1slot: Phaser.Image;
        private P2slot: Phaser.Image;
        private P3slot: Phaser.Image;
        private P4slot: Phaser.Image;
        private P5slot: Phaser.Image;
        private P6slot: Phaser.Image;

        private E1slot: Phaser.Image;
        private E2slot: Phaser.Image;
        private E3slot: Phaser.Image;
        private E4slot: Phaser.Image;
        private E5slot: Phaser.Image;
        private E6slot: Phaser.Image;


        constructor(game: Phaser.Game, x: number, y: number) {
            this.game = game;
            this.drawMainUI();
        }

        public drawMainUI(): void {
            this.MainPUI = this.game.add.sprite(20, 20, "battleUI");
            this.MainPUI.scale.setTo(1.5, 0.8);
            this.MainPUI.fixedToCamera = true;
            this.drawSlot(this.P1slot, PSLOT_ARRAY_X[0], PSLOT_ARRAY_Y[0], true);
            this.drawSlot(this.P2slot, PSLOT_ARRAY_X[1], PSLOT_ARRAY_Y[1], true);
            this.drawSlot(this.P3slot, PSLOT_ARRAY_X[2], PSLOT_ARRAY_Y[2], true);
            this.drawSlot(this.P4slot, PSLOT_ARRAY_X[3], PSLOT_ARRAY_Y[3], true);
            this.drawSlot(this.P5slot, PSLOT_ARRAY_X[4], PSLOT_ARRAY_Y[4], true);
            this.drawSlot(this.P6slot, PSLOT_ARRAY_X[5], PSLOT_ARRAY_Y[5], true);

            this.MainEUI = this.game.add.sprite(1250, 20, "battleUI");
            this.MainEUI.scale.setTo(-1.5, 0.8);
            this.MainEUI.fixedToCamera = true;
            this.drawSlot(this.E1slot, E1SLOT_POS_X, E1SLOT_POS_Y, false);
            this.drawSlot(this.E2slot, E2SLOT_POS_X, E2SLOT_POS_Y, false);
            this.drawSlot(this.E3slot, E3SLOT_POS_X, E3SLOT_POS_Y, false);
            this.drawSlot(this.E4slot, E4SLOT_POS_X, E4SLOT_POS_Y, false);
            this.drawSlot(this.E5slot, E5SLOT_POS_X, E5SLOT_POS_Y, false);
            this.drawSlot(this.E6slot, E6SLOT_POS_X, E6SLOT_POS_Y, false);
        }

        private drawSlot(unit: Phaser.Image, x: number, y: number, temp: boolean): void {
            unit = this.game.add.sprite(x, y, "battleUI");
            if (temp) {
                unit.scale.setTo(0.6, 0.2);
            } else {
                unit.scale.setTo(-0.6, 0.2);
            }
            unit.fixedToCamera = true;
        }
    }
}
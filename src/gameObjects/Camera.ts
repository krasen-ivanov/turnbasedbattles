module TurnBasedBattles {

    export class Cam extends Phaser.Sprite {

        public game: Phaser.Game;
        public animation: Phaser.Sprite;

        constructor(game: Phaser.Game, x: number, y: number) {
            super(game, x, y);
            this.game = game;
            this.x = x;
            this.y = y;
            this.animation = this.game.add.sprite(this.x, this.y, "");
        }

        public update(): void {
            super.update();
        }
    }
}
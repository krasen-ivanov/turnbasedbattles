/// <reference path = "lib/phaser.d.ts"/>

module TurnBasedBattles {
    const GAME_WIDHT: number = 1280;
    const GAME_HEIGHT: number = 720;

    class Battles extends Phaser.Game {

        constructor(width?: number, height?: number) {
            //super(1280 * window.devicePixelRatio, 720 * window.devicePixelRatio, Phaser.CANVAS, "phaser-div");
            //super(window.innerWidth * window.devicePixelRatio, window.innerHeight * window.devicePixelRatio, Phaser.CANVAS, "phaser-div");
            //super(window.innerWidth * 1280, window.innerHeight * 720, Phaser.CANVAS, "phaser-div");
            super(GAME_WIDHT, GAME_HEIGHT, Phaser.CANVAS, "phaser-div");

            this.width = GAME_WIDHT;
            this.height = GAME_HEIGHT;

            this.state.add(BOOT_STATE, Boot, false);
            this.state.add(PRELOAD_STATE, Preload, false);
            this.state.add(MENU_STATE, Menu, false);
            this.state.add(TOWN_STATE, Town, false);
            this.state.add(MAP_STATE, Map, false);
            this.state.add(FIELD_BATTLE_STATE, FieldBattle, false);

            this.state.start(BOOT_STATE);
        }
    }

    window.onload = () => {
        var context: any = new AudioContext();
        let theGame: Battles = new Battles();
    };
}